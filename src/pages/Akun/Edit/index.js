import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  TextInput,
  Alert
} from 'react-native';
import { useForm, Controller } from "react-hook-form";
import { useToast } from "react-native-toast-notifications";
import { setLoading } from '../../../components';
import { saveUserProfile } from '../../../provider';
import {Gap, Button} from '../../../components/atoms';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';


const AkunEdit = ({navigation, route}) => {
    const toast = useToast();
    const { control, handleSubmit, formState: { errors } } = useForm();
    let {userData, updateUserData} = route.params;

    const [images, setImage] = useState('');

    const onSubmit = data => {
        setLoading(true);
        data['photo'] = images != '' ? `data:image/jpeg;base64,${images}` : '';
        saveUserProfile(data).then((res) => {
          updateUserData();
          navigation.goBack();
          toast.show("Update Profil Berhasil")
        }).catch((err) => {
          console.log('error', err);
          toast.show(err.message||'Terjadi Kesalahan');
        }).finally(() =>  setLoading(false));
        
    }

    const getPicture = () => {
      Alert.alert(
        "Ubah Gambar Profil",
        "Pilih Sumber gambar yang akan di upload",
        [
          {text: 'Batal', onPress: () => {}, style: 'cancel'},
          {text: 'Galeri', onPress: () => {
            launchImageLibrary({mediaType: 'photo', includeBase64: true, quality: 0.3}, (res) => {
              if (!res.didCancel) setImage(res.assets[0].base64);
            });
          }},
          {text: 'Kamera', onPress: () => {
            launchCamera({cameraType: 'back', mediaType: 'photo', includeBase64: true, quality: 0.3}, (res) => {
              if (!res.didCancel) setImage(res.assets[0].base64);
            });
          }},
        ]
      )
    }

    return (
        <View style={styles.container}>
          <ImageBackground source={{uri: images != "" ? `data:image/jpg;base64,${images}` : userData?.photo}} style={styles.cover}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={{position: 'absolute', alignSelf: 'flex-start', padding: 7}}>
              <Text style={styles.back}>BACK</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={getPicture} style={{position: 'absolute', alignSelf: 'flex-end', padding: 20}}>
              <Icon name="camera" size={30} color="grey" />
            </TouchableOpacity>
            
          </ImageBackground>
          <View style={styles.content}>
            <ScrollView style={styles.mainContent} showsVerticalScrollIndicator={false}>
              <Text style={styles.title}>No. ID</Text>
              <Text style={styles.subtitle}>{userData?.nik??"-"}</Text>
              <Gap height={20} />
              <Text style={styles.title}>Cabang</Text>
              <Text style={styles.subtitle}>{userData?.branch_name??"-"}</Text>
              <Gap height={20} />
              <Text style={styles.title}>Nama Lengkap</Text>
              {/* <Text style={styles.subtitle}>{userData?.name??"-"}</Text> */}
              <Controller
                control={control}
                rules={{required: true}}
                name="name"
                defaultValue={userData?.name??''}
                render={({field: { onChange, onBlur, value }}) => (
                  <TextInput style={styles.input} onChangeText={onChange} onBlur={onBlur} value={value} />
                )}
              />
              {errors.phone && <Text style={styles.validationText}>This is required.</Text>}
              <Gap height={20} />
              <Text style={styles.title}>Telp</Text>
              <Controller
                control={control}
                rules={{required: true}}
                name="phone"
                defaultValue={userData?.phone??''}
                render={({field: { onChange, onBlur, value }}) => (
                  <TextInput style={styles.input} onChangeText={onChange} onBlur={onBlur} value={value} keyboardType='phone-pad' />
                )}
              />
              {errors.phone && <Text style={styles.validationText}>This is required.</Text>}
              <Gap height={20} />
              <Text style={styles.title}>Alamat</Text>
              <Controller
                control={control}
                rules={{required: true}}
                name="address"
                defaultValue={userData?.address??''}
                render={({field: { onChange, onBlur, value }}) => (
                  <TextInput style={styles.input} onChangeText={onChange} onBlur={onBlur} value={value} numberOfLines={3} textAlignVertical='top' />
                )}
              />
              {errors.address && <Text style={styles.validationText}>This is required.</Text>}
              <Gap height={20} />
              <Text style={styles.title}>Password</Text>
              <Controller
                control={control}
                name="password"
                defaultValue=""
                render={({field: { onChange, onBlur, value }}) => (
                  <TextInput style={styles.input} onChangeText={onChange} onBlur={onBlur} value={value} placeholder="Masukan Password" />
                )}
              />
              <Text style={{...styles.validationText, color: 'grey', fontStyle: 'italic'}}>Note : Kosongkan jika tidak ingin ubah password</Text>
              <Gap height={25} />
              <Button text="Simpan" textColor="white" onPress={handleSubmit(onSubmit)} />
              <Gap height={40} />
            </ScrollView>
          </View>
        </View>
      );
}

export default AkunEdit;

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    back: {
      color: 'grey',
      paddingTop: 26,
      paddingLeft: 22,
      fontWeight: 'bold'
    },
    cover: {
      height: 330,
    },
    content: {
      borderTopRightRadius: 40,
      borderTopLeftRadius: 40,
      marginTop: -30,
      backgroundColor: '#F2F6FF',
      paddingTop: 26,
      paddingHorizontal: 16,
      flex: 1,
    },
    mainContent: {
      padding: 20,
    },
    name: {
      fontFamily: 'Poppins-Medium',
      fontSize: 21,
    },
    title: {
      fontFamily: 'Poppins-Regular',
      fontSize: 13,
    },
    subtitle: {
      fontFamily: 'Poppins-Medium',
      fontSize: 16,
    },
    validationText: {
      fontSize: 10,
      color: 'red',
      marginLeft: 15
    },
    input: {
      borderWidth: 0.1,
      borderRadius: 3,
      borderColor: 'grey',
      backgroundColor: '#F2F6FF',
      paddingHorizontal: 15
    },
  });
  
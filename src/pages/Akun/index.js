import React, {useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ActivityIndicator
} from 'react-native';
import {IcExit, IcPerson, IcTask, ProfileDummy} from '../../assets';
import {clearSession} from '../../utils/session';
import {getUserProfile} from '../../provider';
import {useToast} from 'react-native-toast-notifications';
import { set } from 'react-hook-form';

const Akun = ({navigation}) => {
  const toast = useToast();
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUser();
    return () => {};
  }, [navigation]);

  const getUser = () => {
    getUserProfile()
    .then(setUser)
    .catch((err) => console.error(err))
    .finally(() => setLoading(false));
  }

  const logout = () => {
    Alert.alert('Logout', 'Apakah anda yakin ingin logout?', [
      {text: 'Batal', onPress: () => {}},
      {
        text: 'Logout',
        onPress: () => {
          clearSession();
          navigation.replace('SignIn');
          toast.show('Logout Berhasil');
        },
      },
    ]);
  };

	const toEdit = () => {
		navigation.navigate('AkunEdit', {
      userData: user,
			updateUserData: getUser
		});
	}

  if (loading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color={'#2E2D98'} />
      </View>
    );
  }else {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.akun}>Akun</Text>
          <View style={styles.photo}>
            <View style={styles.borderPhoto}>
              <Image style={{width:97, height: 97, borderRadius: 30}} source={{uri: user?.photo}} />
            </View>
          </View>
          <Text style={styles.name}>{user?.name ?? ''}</Text>
          <TouchableOpacity onPress={toEdit}>
            <View style={styles.dataDiri}>
              <View style={styles.icDatadiri}>
                <IcPerson />
              </View>
              <Text style={styles.text}>Data diri</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <View style={styles.dataDiri}>
              <View style={styles.icDatadiri}>
                <IcTask />
              </View>
              <Text style={styles.text}>Panduan</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={logout}>
            <View style={styles.exitContainer}>
              <View style={styles.icExitContainer}>
                <IcExit />
              </View>
              <Text style={styles.text}>Keluar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

};

export default Akun;

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
  },
  header: {
    padding: 20,
  },
  back: {
    color: '#D32421',
  },
  akun: {
    fontSize: 25,
    color: '#2E2D98',
    fontWeight: 'bold',
    fontFamily: 'Poppins-Medium',
  },
  borderPhoto: {
    borderWidth: 1,
    borderColor: '#8D92A3',
    width: 100,
    height: 100,
    borderRadius: 30,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  photo: {
    marginTop: 5,
    marginBottom: 10,
    padding: 30,
  },
  name: {
    fontSize: 35,
    fontFamily: 'Poppins-Light',
    color: 'black',
  },
  dataDiri: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical:24
  },
  icDatadiri: {
    borderRadius: 10,
    height: 50,
    width: 50,
    marginRight: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F2F6FF',
  },
  text: {
    fontFamily: 'Poppins-Medium',
  },
  icExit: {
    borderRadius: 10,
    height: 50,
    width: 130,
    backgroundColor: '#F2F6FF',

  },
  exitContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 24
  },
  icExitContainer: {
    borderRadius: 10,
    height: 50,
    width: 50,
    marginRight: 30,
    backgroundColor: '#F2F6FF',
    alignItems:'center',
    justifyContent:'center'
  },
});

import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {LogoSplash, Logo} from '../../assets';
import {
  requestMultiple,
  PERMISSIONS,
  RESULTS,
  request,
} from 'react-native-permissions';
import {getUserData} from '../../utils/session';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    permissions();
  }, []);

  const permissions = async () => {
    let permissions = await requestMultiple([
      PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.RECORD_AUDIO,
      PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
    ]);
    for (let i = 0; i < Object.keys(permissions).length; i++) {
      let key = Object.keys(permissions)[i];
      console.log(key, permissions[key]);
      if (permissions[key] != RESULTS.GRANTED) await request(key);
      if (i == Object.keys(permissions).length - 1) checkLogin();
    }
  };

  const checkLogin = async () => {
    const user = await getUserData();
    console.log(user);
    setTimeout(() => {
      if (user === null) {
        navigation.replace('SignIn');
      } else {
        navigation.replace('MainApp');
      }
    }, 1500);
  };

  return (
    <View style={styles.container}>
      <Logo />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

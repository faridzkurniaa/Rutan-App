import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import {Gap} from '../../components/atoms';
import {Button} from '../../components/atoms';
import { getListNotification } from '../../provider';
import { clearSession } from '../../utils/session';
import Icon from 'react-native-vector-icons/FontAwesome5';
const width = Dimensions.get('screen').width / 1 - 30;

const Notif = [
  {
    id: 1,
    userName: 'Pakde Har',
    notifTime: '235/SERVIS/IX/2018',
    notifTitle: 'Start up - Mesin Ketinting',
    notifDate: '17 Mei 2021',
    userImg: require('../../assets/Dummy/pakdehar-pict.jpg'),
  },
  {
    id: 2,
    userName: 'Pakde Har',
    notifTime: '235/SERVIS/IX/2018',
    notifTitle: 'After sales service - Traktor Roda 4',
    notifDate: '17 Mei 2021',
    userImg: require('../../assets/Dummy/pakdehar-pict.jpg'),
  },
  {
    id: 3,
    userName: 'Pakde Har',
    notifTime: '235/SERVIS/IX/2018',
    notifTitle: 'Perbaikan - Filter oli mesin Ketinting',
    notifDate: '17 Mei 2021',
    userImg: require('../../assets/Dummy/pakdehar-pict.jpg'),
  },
];

const Notifikasi = ({navigation}) => {
  const toast = useToast();
  const [lstNotif, setLstNotif] = useState([]);
  const [preload, setPreload] = useState(true);

  useEffect(() => {
    getListNotifications()
    return () => {}
  }, [navigation]);

  const getListNotifications = async () => {
    try {
      setLstNotif(await getListNotification());
      setPreload(false);
    } catch (error) {
      console.log(error);
      setPreload(false);
      if (error == "Invalid access token") {
        clearSession();
        navigation.replace("SignIn");
      }else {
        toast.show(error.toString());
      }
    }
  }


  const Card = ({item}) => {
    return (
      <View style={styles.card}>
        <View style={{flexDirection: 'row'}}>
          <Icon name="bell" color="grey" size={30} />
          <View style={{flex: 1}}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.date}>{item.content}</Text>
          </View>
        </View>
        {/* <View style={styles.bottomContainer}>
          <View style={styles.button}>
            <Button text="Terima" textColor="#2E2D98" color="#F2F6FF" />
          </View>
        </View> */}
      </View>
    );
  };

  if (preload) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text style={styles.back}>Back</Text>
            </TouchableOpacity>
            <Gap height={20} />
            <Text style={styles.akun}>Notifikasi</Text>
          </View>
        </View>  
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator size="large" color="#2E2D98" />
        </View>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={styles.back}>Back</Text>
          </TouchableOpacity>
          <Gap height={20} />
          <Text style={styles.akun}>Notifikasi</Text>
        </View>
      </View>
      <FlatList
        showsVerticalScrollIndicator={true}
        scrollEnabled={true}
        contentContainerStyle={{height: '150%'}}
        numColumns={1}
        data={lstNotif}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return <Card key={item.id} item={item} />;
        }}
      />
    </SafeAreaView>
  );
};

export default Notifikasi;

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
  },
  header: {
    padding: 20,
  },
  back: {
    color: '#D32421',
  },
  akun: {
    fontSize: 25,
    color: '#2E2D98',
    fontWeight: 'bold',
    fontFamily: 'Poppins-Medium',
  },
  card: {
    backgroundColor: 'white',
    marginHorizontal: 12,
    marginTop: 20,
    padding: 10,
    borderRadius: 12,
    elevation: 5,
  },
  notifTime: {
    height: 20,
    fontFamily: 'Poppins-Regular',
    paddingLeft: 7,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 7,
  },
  date: {
    fontFamily: 'Poppins-Regular',
    paddingLeft: 7,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 7,
  },
  leftBottom: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

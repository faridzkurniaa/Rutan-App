import React, {useEffect, useState} from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import { Gap } from '../../../components';
import {lstReject} from '../../../provider';
import { clearSession } from '../../../utils/session';

const RejectPekerjaan = ({navigation, route}) => {
    const toast = useToast();
    let {id} = route.params;
    const [listReject, setListReject] = useState([]);
    const [preload, setPreload] = useState(true);
    const [refresh, setRefresh] = useState(false);
    

    useEffect(() => {
        getListReject();

        return () => {}
    }, [navigation]);

    const getListReject = async () => {
        try {
            const res = await lstReject(id);
            console.log(res);
            setListReject(res);
            setPreload(false);
            setRefresh(false);
        } catch (error) {
            setPreload(false);
            setRefresh(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const renderItem = ({item}) => {
        console.log(item);
        return (
            <View style={{...styles.card, marginHorizontal: 15}}>
                <Text style={{fontWeight: '700'}}>{item.reason}</Text>
                <Text style={{fontSize: 11, color: 'grey'}}>Oleh {item.employee_name}</Text>
            </View>
        );
    }

    if (preload) {
        return (
            <View style={styles.scaffold}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Catatan Reject</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Daftar Reject</Text>
            </View>
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={listReject}
                    renderItem={renderItem}
                    // renderItem={item => {
                    //     return (<View></View>)
                    // }}
                    showsVerticalScrollIndicator={false}
                    refreshing={refresh}
                    onRefresh={() => {
                      setRefresh(true);
                      getListReject();
                    }}
                />
            </View>
        </View>
    );
}

export default RejectPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
        marginBottom: 15,
    },
    container: {
        flex: 1,
    }
});
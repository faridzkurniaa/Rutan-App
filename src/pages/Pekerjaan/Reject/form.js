import React, {useEffect, useState} from 'react';
import { useForm } from 'react-hook-form';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import { Button, Gap, setLoading, TextArea } from '../../../components';
import { insertReject } from '../../../provider';
import { clearSession } from '../../../utils/session';

const FormRejectPekerjaan = ({navigation, route}) => {
    const {id} = route.params;
    const toast = useToast();
    const { control, handleSubmit, formState: { errors } } = useForm();

    useEffect(() => {

        return () => {}
    }, [navigation]);

    const submit = data => {
        setLoading(true);
        insertReject(id, data)
        .then(() => {
          setLoading(false);
          navigation.goBack();
          toast.show("Reject Pekerjaan Berhasil");
        }).catch((err) => {
          console.log(err);
          setLoading(false);
            if (err == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(err.toString());
            }
        });
    }

    return (
        <View styles={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Reject Pekejraan</Text>
            </View>
            <View style={styles.container}>
                <View style={styles.card}>
                    <Text style={styles.text}>Alasan Reject</Text>
                    <TextArea
                        control={control}
                        name="reason"
                        rules={{required: true}}
                        defaultValue=''
                    />
                    {errors.reason && <Text style={styles.validationText}>This is required.</Text>}
                </View>
                <Gap height={20} />
                <Button text="Simpan" textColor="white" onPress={handleSubmit(submit)} />
            </View>
        </View>
    );
}

export default FormRejectPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    container: {
        // flex: 1,
        padding: 10,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15,
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
});
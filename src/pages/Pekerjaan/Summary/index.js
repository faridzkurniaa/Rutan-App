import React, {useEffect, useState} from "react";
import { ActivityIndicator, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { useForm, Controller } from "react-hook-form";
import {Button, Gap, setLoading} from '../../../components';
import {useToast} from 'react-native-toast-notifications';
import { getSmrService, updateSmrService } from "../../../provider";




const SummaryPekerjaan = ({navigation, route}) => {
    let {id} = route.params;
    const toast = useToast();
    const { control, handleSubmit, formState: { errors } } = useForm();
    const [preload, setPreload] = useState(true);
    const [summaryPekerjaan, setSummaryPekerjaan] = useState(null);



    useEffect(() => {
        getSummaryPekerjaan();
    }, [navigation]);

    const getSummaryPekerjaan = async () => {
        try {
            const res = await getSmrService(id);
            setSummaryPekerjaan(res);
            setPreload(false);
        } catch (error) {
            navigation.goBack();
            setPreload(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    const submit = data => {
        setLoading(true);
        updateSmrService(id, data)
        .then(() => {
          setLoading(false);
          navigation.goBack();
          toast.show("Update Summary Berhasil");
        }).catch((err) => {
          console.log(err);
          setLoading(false);
          toast.show(err.toString());
        });
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Summary Pekerjaan</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View styles={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Summary Pekerjaan</Text>
            </View>
            <View style={styles.container}>
                <View style={styles.card}>
                    <Text style={styles.text}>Data Summary</Text>
                    <Controller
                        control={control}
                        rules={{required: true}}
                        name="summary"
                        defaultValue={summaryPekerjaan?.summary??''}
                        render={({field: { onChange, onBlur, value }}) => (
                            <TextInput 
                                placeholder="Masukan Summary Pekerjaan ini" 
                                style={styles.input} 
                                onChangeText={onChange} 
                                onBlur={onBlur} value={value}
                                multiline={true}
                                numberOfLines={4}
                                textAlignVertical='top'
                            />
                        )}
                    />
                    {errors.summary && <Text style={styles.validationText}>This is required.</Text>}
                </View>
                <Gap height={20} />
                <Button text="Simpan" textColor="white" onPress={handleSubmit(submit)} />
            </View>
        </View>
    );
}

export default SummaryPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    container: {
        // flex: 1,
        padding: 10,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15,
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
});
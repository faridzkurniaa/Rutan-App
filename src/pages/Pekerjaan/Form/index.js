import React, {useEffect, useState} from "react";
import { ScrollView, View, Text, TouchableOpacity, StyleSheet, CheckBox, ActivityIndicator, FlatList } from "react-native";
import { Button, ChecklistBox, Gap, setLoading } from "../../../components";
import { getChkService, getDlyService, getMdaService, updateChkService } from "../../../provider";
import {useToast} from 'react-native-toast-notifications';
import { TextInput } from "react-native-gesture-handler";


const FormPekerjaan = ({navigation, route}) => {
    let {id} = route.params;
    const toast = useToast();
    const [checkList, setChecklist] = useState([]);

    const [preload, setPreload] = useState(true);

    const [dataCheckList, setDataCheckList] = useState([]);


    useEffect(() => {
        getDetail();
        return () => {

        }
    }, [navigation]);

    const changeDataCheck = (index, field, value) => {
        let data = [...dataCheckList];
        data[index][field] = value;
        setDataCheckList(data);
    }

    const getDetail = async () => {
        try {
            const res = await getChkService(id);
            var check = [];
            res.forEach(item => {
                var index = res.indexOf(item);
                check.push({
                    "category_form_id": item.category_form_id,
                    "unit_field_id": item.unit_field_id,
                    "is_check": item.is_check,
                    "is_adjust": item.is_adjust,
                    "is_repair": item.is_repair,
                    "is_replace": item.is_replace,
                    "note": item?.note??""
                });
            });
            setDataCheckList(check);
            setChecklist(res);
            setPreload(false);
        } catch (error) {
            setPreload(false);
            navigation.goBack();
            console.log(error);
            toast.show(error.toString());
        }
    }

    const submit = async () => {
        setLoading(true);
        try {
            const res = await updateChkService(id, {checklist: dataCheckList})
            setLoading(false);
            navigation.goBack();
            toast.show("Menyimpan data checklist berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Form Checklist Pekerjaan</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Form Checklist Pekerjaan</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal: 10}}>
                {checkList.map((item)  => {
                    var i = checkList.indexOf(item);
                    return (
                        <View
                            key={item.unit_field_id}
                            style={styles.card}
                        >
                            <Text>({item.category_form_name}) {item.job_form_name}</Text>
                            <ChecklistBox k={`${item.unit_field_id}0`} tipe="Cek Kondisi" value={item['is_check']} onValueChange={(v) => changeDataCheck(i, 'is_check', v)}/>
                            <ChecklistBox k={`${item.unit_field_id}1`} tipe="Melakukan Penyesuaian" value={item['is_adjust']} onValueChange={(v) => changeDataCheck(i, 'is_adjust', v)}/>
                            <ChecklistBox k={`${item.unit_field_id}2`} tipe="Melakukan Perbaikan" value={item['is_repair']} onValueChange={(v) => changeDataCheck(i, 'is_repair', v)}/>
                            <ChecklistBox k={`${item.unit_field_id}3`} tipe="Melakukan Pergantian" value={item['is_replace']} onValueChange={(v) => changeDataCheck(i, 'is_replace', v)}/>
                            <TextInput 
                                placeholder="Catatan"
                                onChangeText={(v) => changeDataCheck(i, 'note', v)}
                                style={styles.input}
                                multiline={true}
                                numberOfLines={3}
                                textAlignVertical='top'
                            />
                        </View>
                    );
                })}
            </ScrollView>
            <Button text="Simpan" textColor="white" style={{margin: 10}} onPress={submit} />
        </View>
    );
}

export default FormPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
        marginBottom: 15,
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
    subText: {
        fontSize: 18,
        color: '#040505',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
    },
    hintText: {
        fontSize: 10,
        color: '#FC9090',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
        textAlign: 'center',
    },
    titleCard: {
        backgroundColor: '#F2F6FF',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    content: {
        // flex: 1,
        paddingHorizontal: 10
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15,
    },
});
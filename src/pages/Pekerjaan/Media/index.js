import React, {useState, useEffect} from 'react';
import { ActivityIndicator, FlatList, Image, StyleSheet, Text, TouchableOpacity, View, Linking } from "react-native";
import { useToast } from 'react-native-toast-notifications';
import { Gap, setLoading } from '../../../components';
import { getMdaService, insertMdaService } from '../../../provider';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from 'react-native-modal';
import {launchCamera} from 'react-native-image-picker';
import ImagePreview from 'react-native-image-preview';

const MediaPekerjaan = ({navigation, route}) => {
    let {id} = route.params;
    const toast = useToast();
    const [dataMedia, setDataMedia] = useState([]);
    const [preload, setPreload] = useState(true);
    const [refresh, setRefresh] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [preview, setPreview] = useState('');
    const [previewModal, setPreviewModal] = useState(false);
    
    useEffect(() => {
        getDataMedia();

        return () => {}
    }, [navigation]);

    const getDataMedia = async () => {
        try {
            const res = await getMdaService(id);
            setDataMedia(res);
            setPreload(false);
            setRefresh(false);
        } catch (error) {
            setPreload(false);
            setRefresh(false);
            navigation.goBack();
            console.log(error);
            toast.show(error.toString());
        }
    }

    const uploadFile = (type) => {
        let config = {
            cameraType: 'back',
            includeBase64: true,
            saveToPhotos: false
        };

        if (type == "video") {
            config['mediaType'] = 'video';
            config['videoQuality'] = 'low';
            config['durationLimit'] = 30;
        }else {
            config['mediaType'] = 'photo';
            config['quality'] = 0.4;
        }

        setShowModal(false);
        launchCamera(config, (res) => {
            if (!res.didCancel) {
                doUpload({
                    base64: `data:image/jpeg;base64,${res.assets[0].base64}`,
                    type,
                    title: null,
                    description: null
                });
            }
        });
    }

    const doUpload = async (data) => {
        setLoading(true);
        try {
            await insertMdaService(id, {
                media: [data]
            });
            setLoading(false);
            setPreload(true);
            await getDataMedia();
            toast.show("Unggah Media Berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    const openVideo = async (url) => {
        const supported = await Linking.canOpenURL(url);
        if (supported) {
            await Linking.openURL(url);
          } else {
            Alert.alert(`Don't know how to open this URL: ${url}`);
          }
    }

    const renderItem = ({item}) => {
        if (item.type == "image") {
            return (
                <TouchableOpacity style={{
                    flex: 1,
                    flexDirection: 'column',
                    margin: 1
                }} onPress={() => {
                    setPreview(item.path);
                    setPreviewModal(true);
                }}>
                    <Image style={styles.imageThumbnail} source={{uri: item.path}} />
                </TouchableOpacity>
            );
        }else {
            return (
                <TouchableOpacity style={{
                    flex: 1,
                    flexDirection: 'column',
                    margin: 1
                }} onPress={() => openVideo(item.path)}>
                    <View style={{...styles.imageThumbnail, backgroundColor: 'black'}}>
                        <Icon name="play" color="white" size={45} />
                    </View>
                </TouchableOpacity>
            );
        }
    }

    
    if (preload) {
        return (
            <View style={{flex: 1, width: '100%', marginHorizontal: 10}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Media Pekerjaan</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Media Pekerjaan</Text>
            </View>
            <View style={{flex: 1, marginHorizontal: 10}}>
                <FlatList
                    keyExtractor={item => item.id}
                    data={dataMedia}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    refreshing={refresh}
                    numColumns={2}
                    onRefresh={() => {
                        setRefresh(true);
                        getDataMedia();
                    }}
                />
            </View>
            <View style={{
                position: 'absolute',
                bottom: 20,
                right: 20
            }}>
                <TouchableOpacity
                    onPress={() => {setShowModal(true)}}
                    style={{
                        height: 60,
                        width: 60,
                        borderRadius: 100,
                        backgroundColor: '#2E2D98',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Icon name='plus' size={25} color='white' />
                </TouchableOpacity>
            </View>
            <Modal
                isVisible={showModal}
                useNativeDriverForBackdrop
                swipeDirection={['down']}
                onSwipeComplete={(v) => setShowModal(false)}
                onBackButtonPress={() => setShowModal(false)}
                style={{justifyContent: 'flex-end',margin: 0}}
            >
                <View style={{
                    backgroundColor: 'white',
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                    height: '30%',
                    width: '100%',
                    alignItems: 'center',
                }}>
                    <View style={{width: '50%', height: 3, backgroundColor: 'grey', marginVertical: 9}} />
                    <Text style={styles.subText}>Pilih Upload</Text>
                    <Gap height={10} />
                    <TouchableOpacity onPress={() => {
                        uploadFile('image');
                    }} style={{
                        width: '100%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderTopWidth: 1,
                        borderBottomWidth: .5,
                        borderTopColor: 'grey',
                        borderBottomColor: 'grey',
                    }}>
                        <Icon name="camera" size={30} color='black' style={{margin: 10}}/>
                        <Text style={{fontSize: 16, fontFamily: 'Poppins-Medium', color: 'black', textAlign: 'center', flex: 1}}>Ambil Gambar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        // uploadFile('video');
                        toast.show("Masih dalam pengembangan")
                    }} style={{
                        width: '100%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderTopWidth: .5,
                        borderBottomWidth: 1,
                        borderTopColor: 'grey',
                        borderBottomColor: 'grey',
                    }}>
                        <Icon name="video" size={30} color='black' style={{margin: 10}}/>
                        <Text style={{fontSize: 16, fontFamily: 'Poppins-Medium', color: 'black', textAlign: 'center', flex: 1}}>Rekam Video</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
            <ImagePreview visible={previewModal} source={{uri: preview}} close={() => {
                setPreviewModal(false);
                setPreview('');
            }} />
        </View>
    );
}

export default MediaPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
        marginBottom: 15,
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
    subText: {
        fontSize: 18,
        color: '#040505',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
    },
    hintText: {
        fontSize: 10,
        color: '#FC9090',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
        textAlign: 'center',
    },
    titleCard: {
        backgroundColor: '#F2F6FF',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    content: {
        paddingHorizontal: 10
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
});
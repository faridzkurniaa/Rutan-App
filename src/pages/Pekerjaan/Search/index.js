import React, {useState} from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import { Gap, ItemListPekerjaan } from '../../../components';
import { searchLstService } from '../../../provider/services_service';
import Icon from 'react-native-vector-icons/FontAwesome5';

const SearchPekerjaan = ({navigation}) => {
    const toast = useToast();
    const [isLoading, setLoading] = useState(false);
    const [keywords, setKeywords] = useState("");
    const [pekerjaan, setPekerjaan] = useState([]);
    const [page, setPage] = useState(1);

    const searchData = async (reset = true) => {
        setLoading(true);
        try {
            const body = {keywords: keywords};
            const res = await searchLstService(body, page);
            console.log("Cari Data", page, res);
            if (page === 1) {
                setPekerjaan(res);
            }else {
                setPekerjaan(pekerjaan.concat(...res));
            }
            setLoading(false);
            setPage(reset === false ? ((page > 1 && res.length > 0) ? (page + 1) : page) : 1);
        } catch (error) {
            navigation.goBack();
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
              }else {
                toast.show(error.toString());
              }
        }
    }

    const renderItem = ({item}) => (
        <TouchableOpacity onPress={() => navigation.navigate("DetailPekerjaan", {id: item.id})} activeOpacity={.8}>
          <ItemListPekerjaan
            data={item}
            onOpenChat={() => navigation.navigate('ChatScreen')}
          />
        </TouchableOpacity>
      );
    

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Cari Pekerjaan</Text>
            </View>
            <View style={styles.searchSection}>
                <Icon style={styles.searchIcon} name="search" size={20} color="grey"/>
                <TextInput
                    style={styles.input}
                    placeholder="Cari Data Pekerjaan"
                    onChangeText={setKeywords}
                    value={keywords}
                    underlineColorAndroid="transparent"
                    returnKeyType={'search'}
                    onSubmitEditing={() => searchData()}
                />
                {keywords != "" && <TouchableOpacity onPress={() => setKeywords("")}>
                    <Icon style={styles.searchIcon} name="times" color="red" size={20} />
                </TouchableOpacity>}
            </View>
            {isLoading === true && <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size="large" color="#2E2D98" /></View>}
            {isLoading === false && 
                <View style={styles.container}>
                   <FlatList
                        keyExtractor={item => item.id?.toString()}
                        data={pekerjaan}
                        renderItem={renderItem}
                        showsVerticalScrollIndicator={false}
                        refreshing={isLoading}
                        onRefresh={searchData}
                        // onEndReachedThreshold={0.5}
                        // onEndReached={({distanceFromEnd}) => console.log(distanceFromEnd)}
                        // onMomentumScrollBegin={() => searchData(false)}
                    />
                </View>
            }
        </View>
    );
}

export default SearchPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1
    },
    container: {
        flex: 1,
        padding: 10,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    searchSection: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
});
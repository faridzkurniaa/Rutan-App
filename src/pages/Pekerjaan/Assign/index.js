import React, {useEffect, useState} from 'react';
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useToast } from 'react-native-toast-notifications';
import { Gap, setLoading } from '../../../components';
import { lstEmployeeService, updEmployeeService } from '../../../provider';
import Icon from 'react-native-vector-icons/FontAwesome5';

const AssignPekerjaan = ({navigation, route}) => {
    const toast = useToast();
    let {id} = route.params;
    const [lstEmployee, setLstEmployee] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [preload, setPreload] = useState(true);

    useEffect(() => {
        const unsubscribeNavigationFocus = navigation.addListener('focus', () => {
            getListEmployee();
        });
        
        return () => {
            unsubscribeNavigationFocus;
        }
    }, [navigation]);

    const getListEmployee = async () => {
        try {
            setLstEmployee(await lstEmployeeService(id));
            setPreload(false);
            setRefresh(false);
        } catch (error) {
            setPreload(false);
            setRefresh(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const updateEmployee = async (job_employee_id, status) => {
        setLoading(true);
        try {
            console.log({job_employee_id, active: (!status).toString()});
            await updEmployeeService(id, {job_employee_id, status: (!status).toString()});
            await getListEmployee();
            setLoading(false);
            toast.show("Update pekerja berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const renderItem = ({item}) => {
        return (
            <View style={{...styles.card, flexDirection: 'row', alignItems: 'center'}}>
                <View style={{flex: 1}}>
                    <Text style={{fontSize: 12, color: 'grey', fontWeight: 'bold'}}>{item.nik}</Text>
                    <Text style={{fontWeight: '600'}}>{item.employee_name}</Text>
                </View>
                <TouchableOpacity style={{padding: 10}} onPress={() => updateEmployee(item.id, item.active)}>
                    <Icon name="power-off" size={20} color={item.active ? 'red':'green'} />
                </TouchableOpacity>
            </View>
        );
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Assign Pekerja</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={{flex: 1}}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Assign Pekerja</Text>
            </View>
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={lstEmployee}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    refreshing={refresh}
                    onRefresh={() => {
                        setRefresh(true);
                        getListEmployee();
                    }}
                />
            </View>
            <View style={{
                position: 'absolute',
                bottom: 10,
                right: 10,
            }}>
                <TouchableOpacity style={styles.floatingBtn} activeOpacity={.6} onPress={() => navigation.navigate('AssignFormPekerjaan', {id, selected: lstEmployee.map((item) => item.employee_id)})}>
                    <Icon name='plus' size={25} color='white' />
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default AssignPekerjaan;

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flex: 1
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 12,
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
    floatingBtn: {
        width: 60,  
        height: 60,   
        backgroundColor: '#2E2D98',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,            
    },
});
import React, {useEffect, useState} from 'react';
import { StyleSheet, TouchableOpacity, View, Text, ActivityIndicator, FlatList,  } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import { Gap, setLoading } from '../../../components';
import { insEmployeeService, lstEmployee } from '../../../provider';
import Icon from 'react-native-vector-icons/FontAwesome5';

const AssignFormPekerjaan = ({navigation, route}) => {
    const toast = useToast();
    let {id, selected} = route.params;
    const [listEmployee, setListEmployee] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [preload, setPreload] = useState(true);

    useEffect(() => {
        console.log(selected)
        getListEmployee();
        return () => {}
    }, [navigation]);

    const getListEmployee = async () => {
        try {
            setListEmployee(await lstEmployee(id));
            setPreload(false);
            setRefresh(false);
        } catch (error) {
            setPreload(false);
            setRefresh(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const selectThisEmployee = async (employee_id) => {
        setLoading(true);
        try {
            await insEmployeeService(id, {employee_id, active: 'true'});
            setLoading(false);
            navigation.goBack();
            toast.show("Tambah pekerja berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const renderItem = ({item}) => {
        return (
            <View style={{...styles.card, flexDirection: 'row', alignItems: 'center'}}>
                <View style={{flex: 1}}>
                    <Text style={{fontSize: 12, color: 'grey', fontWeight: 'bold'}}>{item.nik}</Text>
                    <Text style={{fontWeight: '600'}}>{item.name}</Text>
                    <Text style={{fontSize: 12, color: 'grey', fontWeight: 'bold'}}>Cabang {item.branch_name}</Text>
                </View>
                <TouchableOpacity style={{padding: 10}} onPress={() => selectThisEmployee(item.id)}>
                    <Icon name="plus" size={20} color='green' />
                </TouchableOpacity>
            </View>
        );
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Pilih Pekerja</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (<View style={{flex: 1}}>
        <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Text style={styles.back}>Back</Text>
            </TouchableOpacity>
            <Gap height={20} />
            <Text style={styles.title}>Pilih Pekerja</Text>
        </View>
        <View style={styles.container}>
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={listEmployee.filter((value) => selected.indexOf(value.id) < 0)}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}
                refreshing={refresh}
                onRefresh={() => {
                    setRefresh(true);
                    getListEmployee();
                }}
            />
        </View>
    </View>);
}

export default AssignFormPekerjaan;

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flex: 1
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 12,
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
    floatingBtn: {
        width: 60,
        height: 60,
        backgroundColor: '#2E2D98',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
});


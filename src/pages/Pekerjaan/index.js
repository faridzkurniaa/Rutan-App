import React, {useState, useEffect} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ItemListPekerjaan} from '../../components';
import {getLstService} from '../../provider/services_service';
import {setLoading} from '../../components';
import {useToast} from 'react-native-toast-notifications';
import { clearSession } from '../../utils/session';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Pekerjaan = ({navigation, route}) => {
  const toast = useToast();
  const [tab, setTab] = useState(1);
  const [lstServices, setServices] = useState([]);
  const [page, setPage] = useState(1);
  const [refresh, setRefresh] = useState(false);
  
  useEffect(() => {
    setLoading(true);
    const unsubscribeNavigationFocus = navigation.addListener('focus', () => {
      getServicesData();
    });
    return() => unsubscribeNavigationFocus;
  }, [navigation]);

  const getServicesData = async (reset = true) => {
    try {
      const res = await getLstService(page);
      setServices(res);
      setLoading(false);
      setRefresh(false);
      setPage(reset === false ? (page + 1) : 1);
    } catch (error) {
      setLoading(false);
      setRefresh(false);
      console.log(error);
      if (error == "Invalid access token") {
        clearSession();
        navigation.replace("SignIn");
      }else {
        toast.show(error.toString());
      }
    }
  }


  const renderItem = ({item}) => (
    <TouchableOpacity onPress={() => navigation.navigate("DetailPekerjaan", {id: item.id})} activeOpacity={.8}>
      <ItemListPekerjaan
        data={item}
        onOpenChat={() => navigation.navigate('ChatScreen')}
      />
    </TouchableOpacity>
  );

  const filterData = (value) => {
      if (tab == 1) {
          return value.status == "Progress" || value.status == "Finished";
      } else if (tab == 2) {
          return value.status == "Rejected";
      } else {
          return value.status == "Approved";
      }
  }

  const filterType = (value) => {
      const types = ["Repair","Troubleshoot","Identification"];
      const index = types.indexOf(route?.params?.type??"");
      if (index >= 0) return value.type == types[index];
      return true;
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.pekerjaan}>Pekerjaan</Text>
        <TouchableOpacity activeOpacity={.7} onPress={() => {
            navigation.navigate("SearchPekerjaan")
        }}>
            <Icon name="search" size={20} color="grey" />
        </TouchableOpacity>
      </View>
      <View style={styles.tabContainer}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'space-between',
            // marginHorizontal: 20,
            marginTop: 10,
            marginBottom: 15,
          }}>
          <TouchableOpacity
            style={{paddingVertical: 10, marginHorizontal: 3, width: '30%'}}
            onPress={() => setTab(1)}>
            <Text style={tab == 1 ? styles.tabSelect : styles.tabUnselect}>
              Process
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingVertical: 10, marginHorizontal: 3, width: '30%'}}
            onPress={() => setTab(2)}>
            <Text style={tab == 2 ? styles.tabSelect : styles.tabUnselect}>
              Reject
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingVertical: 10, marginHorizontal: 3, width: '30%'}}
            onPress={() => setTab(3)}>
            <Text style={tab == 3 ? styles.tabSelect : styles.tabUnselect}>
              Complete
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <FlatList
            keyExtractor={item => item.id.toString()}
            data={lstServices.filter(filterType).filter(filterData)}
            renderItem={renderItem}
            showsVerticalScrollIndicator={false}
            refreshing={refresh}
            onRefresh={() => {
              setRefresh(true);
              getServicesData();
            }}
            // onEndReachedThreshold={.3}
            // onEndReached={({distanceFromEnd}) => getServicesData(false)}
          />
        </View>
      </View>
    </View>
  );
};

export default Pekerjaan;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  back: {
    color: '#D32421',
  },
  pekerjaan: {
    fontSize: 25,
    color: '#2E2D98',
    fontWeight: 'bold',
    fontFamily: 'Poppins-Medium',
    flex: 1
  },
  title: {
    fontSize: 25,
    color: '#2E2D98',
    fontWeight: 'bold',
    fontFamily: 'Poppins-Medium',
  },
  tabContainer: {
    flex: 1,
  },
  tabSelect: {
    fontWeight: 'bold',
    color: 'red',
    textAlign: 'center'
  },
  tabUnselect: {
    fontWeight: 'normal',
    color: 'grey',
    textAlign: 'center'
  },
});

import React, {useState, useEffect} from 'react';
import {
    ImageBackground,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ActivityIndicator,
    Touchable
  } from 'react-native';
import { Gap } from '../../../components/atoms';
import {setLoading} from '../../../components';
import {useToast} from 'react-native-toast-notifications';
import {getDtlService, lstEmployeeService, setApproveService, setFinishService, updSerialNumber} from '../../../provider/services_service';
import moment from "moment";
import 'moment/locale/id';
import { monthName } from '../../../utils/global';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DialogInput from 'react-native-dialog-input';
import { getUserData } from '../../../utils/session';

const DetailPekerjaan = ({navigation, route}) => {
    let {id} = route.params;

    const toast = useToast();
    const [preload, setPreload] = useState(true);
    const [detailPekerjaan, setDetailPekerjaan] = useState(null);
    const [employees, setEmployees] = useState([]);
    const [modalSN, setModalSN] = useState(false);
    const [unitID, setUnitID] = useState(null);
    const [role, setRole] = useState([]);


    useEffect(() => {
        const unsubscribeNavigationFocus = navigation.addListener('focus', () => {
            getDetailPekerjaan();
        });
        
        getDataUser();
        return () => {
            unsubscribeNavigationFocus;
        }
    }, [navigation]);

    const colorStatus = (status) => {
        switch (status) {
            case "Rejected":
                return '#C3000D';
            case "Finished":
                return 'green';
            case "Progress":
                return '#0071C5';
            case "Approved":
                return '#00af81';
            default:
                return '#C7C5E6';
        }
    }


    const getDataUser = async () => {
        const user = await getUserData();
        setRole(JSON.parse(user.roles));
    }

    const getDetailPekerjaan = async () => {
        try {
            const res = await getDtlService(id);
            setDetailPekerjaan(res);
            setEmployees(await lstEmployeeService(res.id));
            setPreload(false);
        } catch (error) {
            navigation.goBack();
            setPreload(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    const finishPekerjaan = async () => {
        setLoading(true);
        try {
            await setFinishService(id);
            setLoading(false);
            toast.show("Update Data Pekerjaan Selesai");
            navigation.goBack();
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const approvePekerjaan = async () => {
        setLoading(true);
        try {
            await setApproveService(id);
            setLoading(false);
            toast.show("Update Data Pekerjaan Selesai");
            navigation.goBack();
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const updateSerialNumber = async (serial_number) => {
        setModalSN(false);
        if (unitID == null) return toast.show("Update gagal harap coba lagi");
        setLoading(true);
        try {
            await updSerialNumber(unitID, {serial_number});
            await getDetailPekerjaan();
            setLoading(false);
            setUnitID(null);
            toast.show("Update Serial Number Berhasil");
        } catch (error) {
            setLoading(false);
            setUnitID(null);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
        }
    }

    const ButtonAction = ({icon, title, onPress, disabled=false}) => {
        return (
            <TouchableOpacity 
                activeOpacity={disabled ? 1 : .6}
                style={{
                    flex: 1,
                    backgroundColor: disabled ? 'grey':'#2E2D98',
                    borderRadius: 10,
                    justifyContent: 'center',
                    paddingVertical: 15,
                    marginHorizontal: 10,
                    
                }} 
                onPress={disabled ? () => null : onPress}
            >
                <Icon name={icon} size={40} color='white' style={{textAlign: 'center'}}/>
                <Text style={styles.btnText}>{title}</Text>
            </TouchableOpacity>
        );
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.pekerjaan}>Detail Pekerjaan</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={{flex: 1}}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.pekerjaan}>Detail Pekerjaan</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <View style={styles.content}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.texTitle}>Konsumen</Text>
                        </View>
                        <Gap height={10} />
                        <Text style={styles.text}>Nama (Telp)</Text>
                        <Text style={styles.subText}>{detailPekerjaan.customer_name} ({detailPekerjaan.customer_phone})</Text>
                        <Gap height={10} />
                        <Text style={styles.text}>Alamat</Text>
                        <Text style={styles.subText}>{detailPekerjaan.customer_address}</Text>
                        <Gap height={10} />
                        <Text style={styles.text}>PIC (Telp)</Text>
                        <Text style={styles.subText}>{detailPekerjaan.customer_pic_name} ({detailPekerjaan.customer_pic_phone})</Text>
                    </View>
                    <Gap height={20} />
                    <View style={styles.content}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.texTitle}>Data</Text>
                        </View>
                        <Gap height={10} />
                        <Text style={styles.text}>Job Perform</Text>
                        <Text style={styles.subText}>{detailPekerjaan.job_perform}</Text>
                        <Gap height={10} />
                        <Text style={styles.text}>Lokasi</Text>
                        <Text style={styles.subText}>{detailPekerjaan.location}</Text>
                        <Gap height={10} />
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Tipe</Text>
                                <Text style={styles.subText}>{detailPekerjaan.type}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Status</Text>
                                <Text style={{...styles.subText, color: colorStatus(detailPekerjaan.status), fontWeight: 'bold'}}>{detailPekerjaan.status}</Text>
                            </View>
                        </View>
                        <Gap height={10} />
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Job Form</Text>
                                <Text style={styles.subText}>{detailPekerjaan.job_form_name}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Garansi Hingga</Text>
                                <Text style={styles.subText}>{monthName(detailPekerjaan.warranty_month)} {detailPekerjaan.warranty_year}</Text>
                            </View>
                        </View>
                        <Gap height={10} />
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Start</Text>
                                <Text style={styles.subText}>{moment(detailPekerjaan.start).format('DD MMM YYYY')}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.text}>Due</Text>
                                <Text style={styles.subText}>{moment(detailPekerjaan.due).format('DD MMM YYYY')}</Text>
                            </View>
                        </View>
                    </View>
                    <Gap height={20} />
                    <View style={styles.content}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.texTitle}>Data Perkerja</Text>
                        </View>
                        <Gap height={10} />
                        <Text style={styles.text}>Nama Perkerja (NIK)</Text>
                        {employees.filter((i) => i.active).map(item => {
                            if (employees.length > 1) number = `${employees.indexOf(item) + 1}. `;
                            return (
                                <View key={item.id}>
                                    <Text style={styles.subText}>{(employees.filter((i) => i.active).length > 1) ? `${employees.indexOf(item) + 1}. `:""}{item.employee_name} ({item.nik})</Text>
                                    <Gap height={5} />
                                </ View>
                            );
                        })}
                        <Gap height={10} />
                    </View>
                    {detailPekerjaan.unit.map((item) => {
                        var i = detailPekerjaan.unit.indexOf(item);
                        return (
                            <View style={{...styles.content, marginTop: 20}} key={i}>
                                <View style={styles.titleContainer}>
                                    <Text style={styles.texTitle}>{item.unit_name}</Text>
                                </View>
                                <Gap height={10} />
                                <Text style={styles.text}>Nama Model</Text>
                                <Text style={styles.subText}>{item.unit_model_name}</Text>
                                <Gap height={10} />
                                <Text style={styles.text}>Nomor Serial</Text>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',

                                }}>
                                    <Text style={styles.subText}>{item.serial_number}</Text>
                                    <TouchableOpacity style={{
                                        paddingHorizontal: 3,
                                        marginLeft: 10,
                                        marginBottom: 5
                                    }} onPress={() => {
                                        setUnitID(item.id);
                                        setModalSN(true);
                                    }}>
                                        <Icon name="edit" color="#2E2D98" size={20} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, marginVertical: 10}}/>
                                <View style={{flexDirection: 'row'}}>
                                    <ButtonAction icon="clipboard-check" title="Checklist" onPress={() => {
                                        navigation.navigate('FormPekerjaan', {id: item.id})
                                    }} />
                                    <ButtonAction icon="photo-video" title="Media" onPress={() => {
                                        navigation.navigate('MediaPekerjaan', {id: item.id})
                                    }} />
                                    <ButtonAction icon="clipboard-list" title="Catatan" onPress={() => {
                                        navigation.navigate('CatatanPekerjaan', {id: item.id})
                                    }} />
                                </View>
                            </View>
                        );
                    })}
                    <Gap height={20} />
                    <View style={styles.content}>
                        <View style={{flexDirection: 'row'}}>
                            <ButtonAction icon="window-close" title="Catatan Reject" onPress={() => navigation.navigate('RejectPekerjaan', {id: detailPekerjaan.id})} />
                            <ButtonAction icon="book" title="Summary" onPress={() => {
                                navigation.navigate('SummaryPekerjaan', {id: detailPekerjaan.id})
                            }} />
                            <ButtonAction icon="user-check" title="Selesai" onPress={() => finishPekerjaan()}  disabled={detailPekerjaan.status === 'Finished' || detailPekerjaan.status === 'Approved'}/>
                        </View>
                        {role.indexOf("supervisor") >= 0 && 
                            <View style={{flexDirection: 'row', marginTop: 10}}>
                            <ButtonAction icon="users" title="Assign Pekerja" onPress={() => navigation.navigate('AssignPekerjaan', {id: detailPekerjaan.id})} />
                            <ButtonAction icon="window-close" title="Reject"onPress={() => navigation.navigate('FormRejectPekerjaan', {id: detailPekerjaan.id})} disabled={detailPekerjaan.status !== 'Approved'}/>
                            <ButtonAction icon="check-circle" title="Approve" onPress={() => approvePekerjaan()} disabled={detailPekerjaan.status !== 'Finish'}/>
                        </View>
                        }
                    </View>
                    <Gap height={20} />
                </View>
            </ScrollView>
            <DialogInput isDialogVisible={modalSN}
                title={"Nomor Serial"}
                message={"Masukan Nomor Serial Mesin"}
                hintInput ={"Nomor Serial"}
                submitInput={ (inputText) => updateSerialNumber(inputText) }
                closeDialog={ () => {
                    setUnitID(null);
                    setModalSN(false);
                }}
            />
        </View>
    );
}

export default DetailPekerjaan;

const styles = StyleSheet.create({
    container: {
        padding: 5,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    pekerjaan: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    content: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
    subText: {
        fontSize: 18,
        color: '#040505',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
    },
    hintText: {
        fontSize: 10,
        color: '#FC9090',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
        textAlign: 'center',
    },
    checkboxContainer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    checkbox: {
        alignContent: 'center',
    },
    label: {
        margin: 8,
        marginRight: 5,
    },
    title: {
        fontSize: 16,
        color: '#040505',
        fontFamily: 'Poppins-Medium',
        marginLeft: 5,
    },
    button: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    cover: {
        height: 330,
    },
    titleContainer: {
        backgroundColor: '#F2F6FF',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    btnContainer: {
        flex: 1,
        backgroundColor: '#2E2D98',
        borderRadius: 10,
        justifyContent: 'center',
        paddingVertical: 15
        // paddingHorizontal: 30,
        // width: 145,
        // height: 50,
    },
    btnText: {
        color: 'white',
        textAlign: 'center',
    },
});
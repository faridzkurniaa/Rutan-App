import React, {useState, useEffect} from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View, Alert } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import { Gap, IconButton, setLoading } from '../../../components';
import { deleteDlyService, getDlyService } from '../../../provider';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from "moment";
import 'moment/locale/id';


const CatatanPekerjaan = ({navigation, route}) => {
    let {id} = route.params;
    const toast = useToast();

    const [dataNote, setDataNote] = useState([]);
    const [preload, setPreload] = useState(true);
    const [refresh, setRefresh] = useState(false);

    useEffect(() => {
        moment().locale('id');
        const unsubscribeNavigationFocus = navigation.addListener('focus', () => {
            getDataNote();
        });
        
        return () => {
            unsubscribeNavigationFocus;
        }
    }, [navigation]);

    const getDataNote = async () => {
        try {
            const res = await getDlyService(id);
            setDataNote(res);
            setPreload(false);
            setRefresh(false);
        } catch (error) {
            setPreload(false);
            setRefresh(false);
            navigation.goBack();
            console.log(error);
            toast.show(error.toString());
        }
    }

    const deleteNote = async (idDaily) => {
        Alert.alert("Hapus Catatan Ini", "Apakah anda yakin?",
        [
            { text: "Batal" },
            { text: "Hapus", onPress: () => actionDelete(idDaily) }
        ])
    }

    const actionDelete = async (idDaily) => {
        setLoading(true);
        try {
            await deleteDlyService(id, idDaily);
            setLoading(false);
            setPreload(true);
            await getDataNote();
            toast.show("Hapus Catatan Berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    const renderItem = ({item}) => (
        <View 
            style={styles.card}
        >
          <Text style={styles.text}>{item.title}</Text>
          <Text style={styles.subText}>{item.description}</Text>
          <Gap height={10} />
          <Text style={{...styles.text, fontSize: 12, color: 'grey'}}>{moment(item.daily_start).format('DD MMM YYYY HH:ss')} s/d {moment(item.daily_end).format('DD MMM YYYY HH:ss')}</Text>
          <IconButton 
            icon="trash" 
            color="red" 
            style={{position: 'absolute', top: 5, right: 5}}
            iconSize={20}
            onPress={() => deleteNote(item.id)}
        />
        </View>
      );

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Catatan Pekerjaan</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Catatan Pekerjaan</Text>
            </View>
            <View style={{flex: 1, marginHorizontal: 10}}>
                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={dataNote}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    refreshing={refresh}
                    onRefresh={() => {
                        setRefresh(true);
                        getDataNote();
                    }}
                />
            </View>
            <View style={{
                position: 'absolute',
                bottom: 20,
                right: 20
            }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("FormCatatanPekerja", {id})}
                    style={{
                        height: 60,
                        width: 60,
                        borderRadius: 100,
                        backgroundColor: '#2E2D98',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Icon name='plus' size={25} color='white' />
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default CatatanPekerjaan;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
        marginBottom: 15,
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
    subText: {
        fontSize: 18,
        color: '#040505',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
    },
    hintText: {
        fontSize: 10,
        color: '#FC9090',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
        textAlign: 'center',
    },
    titleCard: {
        backgroundColor: '#F2F6FF',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    content: {
        paddingHorizontal: 10
    }
});
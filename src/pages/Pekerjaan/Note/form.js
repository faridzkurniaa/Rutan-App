import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Button, Gap, IconButton, setLoading } from '../../../components';
import { useForm, Controller } from "react-hook-form";
import DatePicker from 'react-native-date-picker';
import moment from "moment";
import 'moment/locale/id';
import { insertDlyService } from '../../../provider';
import { useToast } from 'react-native-toast-notifications';


const FormCatatanPekerja = ({navigation, route}) => {
    let {id} = route.params;
    const toast = useToast();
    const { control, handleSubmit, setValue, formState: { errors } } = useForm();
    const [show, setShow] = useState(false);
    const [dateStart, setDateStart] = useState(new Date());
    const [dateEnd, setDateEnd] = useState(new Date());
    const [dateType, setDateType] = useState('');

    useEffect(() => {
        moment().locale('id');


        return () => {

        }
    }, [navigation]);

    const submit = async data => {
        setLoading(true);
        data['daily_start'] = moment(dateStart).format('YYYY-MM-DD hh:mm:ss');
        data['daily_end'] = moment(dateEnd).format('YYYY-MM-DD hh:mm:ss');

        try {
            await insertDlyService(id, data);
            setLoading(false);
            navigation.goBack();
            toast.show("Menambahkan Catatan Berhasil");
        } catch (error) {
            setLoading(false);
            console.log(error);
            toast.show(error.toString());
        }
    }

    const datePicker = (type) => {
        setDateType(type);
        setShow(true);
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Form Catatan Pekerjaan</Text>
            </View>
            <View style={styles.card}>
                <Text style={styles.text}>Judul Catatan</Text>
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="title"
                    defaultValue={''}
                    render={({field: { onChange, onBlur, value }}) => (
                        <TextInput 
                            placeholder="Judul" 
                            style={styles.input} 
                            onChangeText={onChange} 
                            onBlur={onBlur} value={value}
                        />
                    )}
                />
                {errors.title && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={10} />
                <Text style={styles.text}>Waktu Mulai</Text>
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="daily_start"
                    defaultValue={''}
                    render={({field: { onChange, onBlur, value }}) => (
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <TextInput 
                                    placeholder="Mulai"
                                    editable={false}
                                    style={styles.input} 
                                    onChangeText={onChange} 
                                    onBlur={onBlur} value={value}
                                />
                            </View>
                            <IconButton icon="calendar-alt" onPress={() => datePicker('start')}/>
                        </View>
                    )}
                />
                {errors.daily_start && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={10} />
                <Text style={styles.text}>Waktu Selesai</Text>
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="daily_end"
                    defaultValue={''}
                    render={({field: { onChange, onBlur, value }}) => (
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <TextInput 
                                    placeholder="Selesai"
                                    editable={false}
                                    style={styles.input} 
                                    onChangeText={onChange} 
                                    onBlur={onBlur} value={value}
                                />
                            </View>
                            <IconButton icon="calendar-alt" onPress={() => datePicker('end')}/>
                        </View>
                    )}
                />
                {errors.daily_end && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={10} />
                <Text style={styles.text}>Keterangan</Text>
                <Controller
                    control={control}
                    name="description"
                    defaultValue={''}
                    render={({field: { onChange, onBlur, value }}) => (
                        <TextInput 
                            placeholder="Deskripsi" 
                            style={styles.input} 
                            onChangeText={onChange} 
                            onBlur={onBlur} value={value}
                            multiline={true}
                            numberOfLines={4}
                            textAlignVertical='top'
                        />
                    )}
                />
            </View>
            <Button text="Simpan" textColor="white" style={{margin: 10}} onPress={handleSubmit(submit)} />
            <DatePicker
                modal
                open={show}
                date={dateType == "start" ? dateStart: dateEnd}
                onConfirm={(date) => {
                    setShow(false)
                    if (dateType == "start") {
                        setDateStart(date);
                        setValue('daily_start', moment(date).format('DD MMM YYYY HH:ss'));
                    }else {
                        setDateEnd(date);
                        setValue('daily_end', moment(date).format('DD MMM YYYY HH:ss'));
                    }
                    setDateType("");
                }}
                onCancel={() => {
                    setDateType("");
                    setShow(false)
                }}
            />
        </View>
    );
}

export default FormCatatanPekerja;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    header: {
        padding: 20,
    },
    back: {
        color: '#D32421',
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15,
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    card: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        elevation: 10,
        marginBottom: 15,
        marginHorizontal: 10
    },
    text: {
        fontSize: 14,
        color: '#040505',
        fontFamily: 'Poppins-Light',
        marginLeft: 5,
    },
    subText: {
        fontSize: 18,
        color: '#040505',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
    },
    hintText: {
        fontSize: 10,
        color: '#FC9090',
        fontFamily: 'Poppins-Regular',
        marginLeft: 5,
        textAlign: 'center',
    },
    titleCard: {
        backgroundColor: '#F2F6FF',
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
    },
    texTitle: {
        textAlign: 'center',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        color: '#2E2D98',
        fontWeight: 'bold',
    },
    content: {
        paddingHorizontal: 10
    }
});
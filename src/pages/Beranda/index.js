import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  Banner,
  IcAfterSales,
  IcNotif,
  IcPerbaikan,
  IcStartup,
  LogoPt,
} from '../../assets';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Beranda = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <LogoPt width="40%"/>
        <TouchableOpacity onPress={()=> navigation.navigate('Notifikasi')} >
          <Icon name="notifications" size={30}/>
        </TouchableOpacity>
      </View>
      <Banner width="100%"/>
      <View style={styles.serviceContainer}>
        <TouchableOpacity style={{ width: '30%'}} onPress={()=> navigation.navigate('Pekerjaan', {type: 'Identification'})}>
          <View style={styles.iconContainer}>
          <IcStartup />
          </View>
          <Text style={styles.text}>Identification</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ width: '30%'}} onPress={()=> navigation.navigate('Pekerjaan', {type: 'Troubleshoot'})} >
          <View style={styles.iconContainer}>
          <IcAfterSales />
          </View>
          <Text style={styles.text}>Troubleshoot</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ width: '30%' }} onPress={()=> navigation.navigate('Pekerjaan', {type: 'Repair'})} >
          <View style={styles.iconContainer}>
          <IcPerbaikan />
          </View>
          <Text style={styles.text}>Repair</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Beranda;

const styles = StyleSheet.create({
  header: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  container: {
    marginTop: 10,
    padding: 20,
  },
  serviceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 30,
  },
  text: {
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  iconContainer: { 
    backgroundColor:'#F2F6FF', 
  borderRadius:17,
  alignItems: 'center', 
  width:100,
  height:100 }
});

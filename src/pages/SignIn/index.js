import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View, TouchableOpacity} from 'react-native';
import {Logo} from '../../assets';
import {Button, Gap} from '../../components/atoms';
import { useForm, Controller } from "react-hook-form";
import { loginApi } from '../../provider';
import { useToast } from "react-native-toast-notifications";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { setLoading } from '../../components';

const SignIn = ({navigation}) => {
  const toast = useToast();
  const { control, handleSubmit, formState: { errors } } = useForm();
  const [showPassword, setShowPassword] = useState(false);

  const onSubmit = data => {
    setLoading(true);
    loginApi(data.nik, data.password)
    .then(() => {
      setLoading(false);
      navigation.replace('MainApp');
    }).catch((err) => {
      console.log(err);
      setLoading(false);
      toast.show(err.toString());
    });
  }

  return (
    <View style={styles.container}>
      <Text style={styles.login}>Login</Text>
      <View style={styles.logoContainer}>
        <Logo />
      </View>
      <View>
        <Controller
          control={control}
          rules={{required: true}}
          name="nik"
          defaultValue="2013310017"
          render={({field: { onChange, onBlur, value }}) => (
            <TextInput placeholder="NIK" style={styles.input} onChangeText={onChange} onBlur={onBlur} value={value} keyboardType = 'numeric' />
          )}
        />
        {errors.nik && <Text style={styles.validationText}>This is required.</Text>}
        <Gap height={10} />
        <Controller
          control={control}
          rules={{required: true}}
          name="password"
          defaultValue="sandi123"
          render={({field: { onChange, onBlur, value }}) => (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TextInput placeholder="Password" style={[styles.input, {flex: 1}]} onChangeText={onChange} onBlur={onBlur} value={value} secureTextEntry={!showPassword} />
              <TouchableOpacity style={{position: 'absolute', right: 0, padding: 20}} onPress={() => setShowPassword(!showPassword)}>
                <Icon name={showPassword ? "eye-slash" : "eye"} size={15} color="#2E2D98" />
              </TouchableOpacity>
            </View>
          )}
        />
        {errors.password && <Text style={styles.validationText}>This is required.</Text>}
      </View>
      <Gap height={20} />
      <Button text="Masuk" textColor="white" onPress={handleSubmit(onSubmit)} />
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  container: {
    padding: 25,
    flex: 1
  },
  login: {
    color: '#2E2D98',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 70,
    fontFamily:'Poppins-Regular'
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  input: {
    borderWidth: 0.1,
    borderRadius: 3,
    borderColor: 'grey',
    backgroundColor: '#F2F6FF',
    paddingHorizontal: 15
  },
  validationText: {
    fontSize: 10,
    color: 'red',
    marginLeft: 15
  }
});

import SplashScreen from "./SplashScreen";
import SignIn from "./SignIn";
import Akun from "./Akun";
import AkunEdit from "./Akun/Edit";
import AkunPanduan from "./Akun/Panduan";

import Beranda from "./Beranda";
import Pekerjaan from "./Pekerjaan";
import DetailPekerjaan from "./Pekerjaan/Detail";
import FormPekerjaan from "./Pekerjaan/Form";
import SummaryPekerjaan from "./Pekerjaan/Summary";
import CatatanPekerjaan from "./Pekerjaan/Note";
import FormCatatanPekerja from "./Pekerjaan/Note/form";
import SearchPekerjaan from './Pekerjaan/Search';
import MediaPekerjaan from "./Pekerjaan/Media";
import RejectPekerjaan from "./Pekerjaan/Reject";
import FormRejectPekerjaan from "./Pekerjaan/Reject/form";
import AssignPekerjaan from './Pekerjaan/Assign';
import AssignFormPekerjaan from './Pekerjaan/Assign/form';

import Pesan from "./Pesan";
import ChatScreen from "./ChatScreen";
import Notifikasi from "./Notifikasi";
import AfterSales from "./AfterSales";
import Perbaikan from "./Perbaikan";

import Identification from './Identifikasi';
import IdentificationDetail from './Identifikasi/Detail';
import IdentificationForm from './Identifikasi/Form';


export {
    SplashScreen,
    SignIn,
    Akun,
    AkunEdit,
    AkunPanduan,
    Beranda,
    Pekerjaan,
    DetailPekerjaan,
    FormPekerjaan,
    SummaryPekerjaan,
    CatatanPekerjaan,
    FormCatatanPekerja,
    MediaPekerjaan,
    SearchPekerjaan,
    Pesan,
    ChatScreen,
    Notifikasi,
    AfterSales,
    Perbaikan,
    Identification,
    IdentificationDetail,
    IdentificationForm,
    RejectPekerjaan,
    FormRejectPekerjaan,
    AssignPekerjaan,
    AssignFormPekerjaan,
}
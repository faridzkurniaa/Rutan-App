import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, FlatList } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getLstIdentification } from '../../provider';
import moment from "moment";
import 'moment/locale/id';

const Identification = ({navigation}) => {
    const toast = useToast();
    const [lstIdentification, setLstIdentification] = useState([]);
    const [preload, setPreload] = useState(true);
    const [refresh, setRefresh] = useState(false);
    const [page, setPage] = useState(1);

    
    useEffect(() => {
        moment().locale('id');
        const unsubscribeNavigationFocus = navigation.addListener('focus', () => {
            getListIdentification();
        });
        return () => {
            unsubscribeNavigationFocus;
        }
    }, [navigation]);

    const colorStatus = (status) => {
        switch (status) {
            case "Rejected":
                return '#C3000D';
            case "Finished":
                return 'green';
            case "Progress":
                return '#0071C5';
            case "Approved":
                return '#00af81';
            default:
                return '#C7C5E6';
        }
    }


    const getListIdentification = async (reset = true) => {
        try {
            const res = await getLstIdentification(page);
            setLstIdentification(res);
            setPreload(false);
            setRefresh(false);
            setPage(reset === false ? (page + 1) : 1);
          } catch (error) {
            setPreload(false);
            setRefresh(false);
            console.log(error);
            if (error == "Invalid access token") {
              clearSession();
              navigation.replace("SignIn");
            }else {
              toast.show(error.toString());
            }
          }
    }

    const renderItem = ({item}) => {
        return (
            <TouchableOpacity activeOpacity={.7} style={styles.card} onPress={() => navigation.navigate('IdentificationDetail', {id: item.id})}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                        <Text>{item.no_identification}</Text>
                        <Text>{item.type}</Text>
                        <View style={{flexDirection: 'row', marginTop: 10}}>
                            <Icon name="user-tie" size={15} style={{marginRight: 5}}/>
                            <Text>{item.customer_name} ({item.branch_name})</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Icon name="map-marker-alt" size={15} style={{marginRight: 5}}/>
                            <Text>{item.location}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Icon name="stopwatch" size={12} style={{marginRight: 5, color: 'grey'}}/>
                            <Text style={{fontSize: 10, color: 'grey'}}>{moment(item.created_date).format('DD MMM YYYY HH:mm')} WIB</Text>
                        </View>
                    </View>
                    <View>
                        <View style={{...styles.statusLabel, backgroundColor: colorStatus(item.status)}}>
                            <Text style={{color: 'white'}}>{item.status}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    if (preload) {
        return (
            <View style={{flex: 1}}>
                <View style={styles.header}>
                    <Text style={styles.title}>Identifikasi</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <Text style={styles.title}>Identifikasi</Text>
                <TouchableOpacity activeOpacity={.7} onPress={() => {
                    navigation.navigate("SearchPekerjaan")
                }}>
                    <Icon name="search" size={20} color="grey" />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={lstIdentification}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    refreshing={refresh}
                    onRefresh={() => {
                        setRefresh(true);
                        getListIdentification();
                    }}
                    // onEndReachedThreshold={.3}
                    // onEndReached={({distanceFromEnd}) => getServicesData(false)}
                />
            </View>
            <View style={{
                position: 'absolute',
                bottom: 10,
                right: 10,
            }}>
                <TouchableOpacity style={styles.floatingBtn} activeOpacity={.6} onPress={() => navigation.navigate('IdentificationForm', {id: null})}>
                    <Icon name='plus' size={25} color='white' />
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default Identification;

const styles = StyleSheet.create({
    scaffold: {
      flex: 1 ,
      position: 'relative'
    },
    container: {
        flex: 1,
        // backgroundColor: 'grey'
    },
    header: {
        flexDirection: 'row',
        paddingTop: 20,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
        flex: 1
    },
    floatingBtn: {
        width: 60,  
        height: 60,   
        backgroundColor: '#2E2D98',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,            
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 12,
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
    statusLabel: {
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 5
    }
});
import React from "react";
import { ScrollView, StyleSheet, Text } from "react-native";
import { Dropdown, Gap, Input, TextArea } from "../../../components";
import ChangeSparePart from "./change_sparepart";
import SellingSpareParts from "./selling_sparepart";
import NeedSpareParts from "./needs_sparepart";

const RegularPage = ({data, control, errors, setValue}) => {

    return (
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
            <Text>Jenis Instansi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Dropdown
                control={control}
                rules={{required: true}}
                name="instance_type"
                defaultValue={data?.regulars?.instance_type??""}
                data={[
                    {label: "Pilih", value: ""},
                    {label: "Kepemilikan Pribadi", value: "T1"},
                    {label: "Perusahaan", value: "T2"},
                    {label: "Kelompok Tani", value: "T3"},
                    {label: "Dinas", value: "T4"},
                ]}
            />
            {errors.instance_type && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nama Instansi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input 
                control={control} 
                rules={{required: true}} 
                name="instance_name"
                defaultValue={data?.regulars?.instance_name??""}
            /> 
            {errors.instance_name && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nama<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="name"
                defaultValue={data?.regulars?.name??""}
            />
            {errors.name && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nomor KTP/NPWP<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="ktp_npwp"
                defaultValue={data?.regulars?.ktp_npwp??""}
            />
            {errors.ktp_npwp && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Status<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Dropdown
                control={control}
                rules={{required: true}}
                name="status"
                defaultValue={data?.regulars?.status??""}
                data={[
                    {label: "Pilih", value: ""},
                    {label: "Pemilik", value: "S1"},
                    {label: "Operator", value: "S2"},
                    {label: "Penanggung Jawab", value: "S3"},
                    {label: "Lainnya", value: "S4"}
                ]}
            />
            {errors.status && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Kelurahan</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="address_subdistrict"
                defaultValue={data?.regulars?.address_subdistrict??""}
            />
            <Gap height={20} />
            <Text>Kecamatan</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="address_district"
                defaultValue={data?.regulars?.address_district??""}
            />
            <Gap height={20} />
            <Text>Kabupaten / Kota<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="address_city"
                defaultValue={data?.regulars?.address_city??""}
            />
            {errors.address_city && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Provinsi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="address_province"
                defaultValue={data?.regulars?.address_province??""}
            />
            {errors.address_province && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Kode Pos</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="address_postal_code"
                defaultValue={data?.regulars?.address_postal_code??""}
                keyboardType="number-pad"
            />
            <Gap height={20} />
            <Text>Nomor Telepon<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="phone"
                defaultValue={data?.regulars?.phone??""}
            />
            {errors.phone && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nama Produk<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="product_name"
                defaultValue={data?.regulars?.product_name??""}
            />
            {errors.product_name && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Tanggal Pembelian</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="buy_date"
                defaultValue={data?.regulars?.buy_date??""}
            />
            <Gap height={20} />
            <Text>Tanggal Terima Bantuan</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="assistance_date"
                defaultValue={data?.regulars?.assistance_date??""}
            />
            <Gap height={20} />
            <Text>Nomor Seri<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="serial_number"
                defaultValue={data?.regulars?.serial_number??""}
            />
            {errors.serial_number && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nomor Mesin<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="machine_number"
                defaultValue={data?.regulars?.machine_number??""}
            />
            {errors.machine_number && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nomor Gearbox<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="gearbox_number"
                defaultValue={data?.regulars?.gearbox_number??""}
            />
            {errors.gearbox_number && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Tahun Produksi Unit</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="production_unit_year"
                defaultValue={data?.regulars?.production_unit_year??""}
            />
            <Gap height={20} />
            <Text>Jam Kerja (per hari)</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="work_hour_per_day"
                defaultValue={data?.regulars?.work_hour_per_day??""}
            />
            <Gap height={20} />
            <Text>Jam Kerja (Hour Meter)<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="hour_meter"
                defaultValue={data?.regulars?.hour_meter??""}
            />
            {errors.hour_meter && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Riwayat Perbaikan</Text>
            <Gap height={10} />
            <Input
                control={control}
                placeholder="Tempat Perbaikan"
                name="history_service_place"
                defaultValue={data?.regulars?.history_service_place??""}
            />
            <Gap height={10} />
            <Input
                control={control}
                placeholder="Jenis Perbaikan"
                name="history_service_type"
                defaultValue={data?.regulars?.history_service_type??""}
            />
            <Text style={{...styles.validationText, color: 'grey'}}>Kosongkan jika belum pernah dilakukan perbaikan.</Text>
            <Gap height={20} />
            <ChangeSparePart control={control} data={data}/>
            <Gap height={20} />
            <SellingSpareParts control={control} data={data} />
            <Gap height={20} />
            <NeedSpareParts control={control} data={data} />
            <Gap height={20} />
            <Text>Catatan</Text>
            <Gap height={10} />
            <TextArea
                control={control}
                name="note"
                defaultValue={data?.regulars?.note??""}
            />
            <Gap height={20} />
        </ScrollView>
    );
}

export default RegularPage;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    requireLbl: {
        color: 'red',
        fontSize: 11
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});
import React, {useState, useEffect} from "react";
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useToast } from "react-native-toast-notifications";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Gap, Input } from "../../../components";
import { getListConfEngine } from "../../../provider/indentification_service";

const ConfigEngine = ({control, data, setValue}) => {
    const toast = useToast();
    const [engineConfs, setEngineConfs] = useState([]);
    const [preload, setPreload] = useState(true);

    useEffect(() => {
        setEngineConfs([]);
        getEngineConfs();
        return () => {
            setEngineConfs([]);
        }
    }, [control, data]);

    const getEngineConfs = async () => {
        try {
            const res = await getListConfEngine();
            const dataConfs = [...res.map((value) => {
                return {
                    engine_conf_id: value.id,
                    buy_and_use_year: "",
                    serial_number: "",
                    qty: 0
                };
            })];

            if (data.milling_confs.length > 0) {
                data.milling_confs.forEach((v) => {
                    var index = dataConfs.findIndex((z) => z.engine_conf_id == v.engine_conf_id);
                    setValue(`engine_confs.${index}.engine_conf_id`, v.engine_conf_id);
                    dataConfs[index]["buy_and_use_year"] = v.buy_and_use_year;
                    dataConfs[index]["serial_number"] = v.serial_number;
                    dataConfs[index]["qty"] = v.qty;
                })
            }
            setEngineConfs(dataConfs);
            setPreload(false);
        } catch (error) {
            setPreload(false);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
            }else {
                toast.show(error.toString());
            }
        }
    }

    if (preload) {
        return (
            <View>
                <Text>Config Engine</Text>
                <View>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );    
    }
    
    return (
        <View>
            <Text>Konfigurasi Mesin</Text>
            {engineConfs.map((value, key) => {
                return (
                    <View style={styles.card} key={`conf${key}`}>
                        <Input control={control} placeholder="Tahun Pembelian & Pemakaian" name={`engine_confs.${key}.buy_and_use_year`} defaultValue={value?.buy_and_use_year??""} keyboardType='number-pad'/>
                        <Gap height={10} />
                        <Input control={control} placeholder="Nomor Seri" name={`engine_confs.${key}.serial_number`} defaultValue={value?.serial_number??""}/>
                        <Gap height={10} />
                        <Input control={control} placeholder="Jumlah" name={`engine_confs.${key}.qty`} defaultValue={value?.qty?.toString()??""} keyboardType='number-pad'/>
                    </View>
                );
            })}
        </View>
    );
}

export default ConfigEngine;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});
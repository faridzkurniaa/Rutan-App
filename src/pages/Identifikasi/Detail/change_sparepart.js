import React, {useEffect} from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Gap, Input } from "../../../components";
import { useFieldArray } from 'react-hook-form';

const ChangeSparePart = ({control, data}) => {
    const {fields, append, remove} = useFieldArray({
        control,
        name: "spare_part_changing_histories"
    });

    useEffect(() => {
        populateHistorySparePart();
        return () => {}
    }, [data]);

    const populateHistorySparePart = () => {
        let {spare_part_changing_histories} = data;
        if (spare_part_changing_histories != null) {
            spare_part_changing_histories.forEach((item) => {
                setTimeout(() => {
                    append({
                        "spare_part_name": item.spare_part_name,
                        "spare_part_buy_place": item.spare_part_buy_place
                    });
                }, 500);
            });
        }
    }

    return (
        <>
            <View style={{
                flexDirection: 'row'
            }}>
                <Text style={{flex: 1}}>Riwayat Pergantian Spare part</Text>
                <TouchableOpacity activeOpacity={.7} onPress={() => {
                    append({
                        "spare_part_name": "",
                        "spare_part_buy_place": ""
                    });
                }}>
                    <Icon name="plus" size={20} color="#2E2D98" />
                </TouchableOpacity>
            </View>
            <Gap height={10} />
            {fields.map((value, key) => {
                return (
                    <View style={styles.card} key={`history${key}`}>
                        <View style={{
                            flexDirection: 'row',
                            paddingHorizontal: 10
                        }}>
                            <Text style={{flex: 1}}>Spare Part {key+1}</Text>
                            <TouchableOpacity activeOpacity={.7} onPress={() => remove(key)}>
                                <Icon name="trash" color="red" size={15}/>
                            </TouchableOpacity>
                        </View>
                        <Gap height={10} />
                        <Input control={control} placeholder="Nama Spare part" name={`spare_part_changing_histories.${key}.spare_part_name`} defaultValue={value?.spare_part_name??""} />
                        <Gap height={10} />
                        <Input control={control} placeholder="Tempat Pembelian" name={`spare_part_changing_histories.${key}.spare_part_buy_place`} defaultValue={value?.spare_part_buy_place??""} />
                    </View>
                );
            })}
        </>
    );
}


export default ChangeSparePart;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});
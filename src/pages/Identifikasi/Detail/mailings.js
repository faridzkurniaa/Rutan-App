import React from "react";
import { ScrollView, StyleSheet, Text } from "react-native";
import { Dropdown, Gap, Input, TextArea } from "../../../components";
import ChangeSparePart from "./change_sparepart";
import ConfigEngine from "./conf_engine";
import SellingSpareParts from "./selling_sparepart";
import NeedSpareParts from "./needs_sparepart";

const MillingPage = ({data, control, errors, setValue}) => {
    let {detailIdent, engineConf} = data;

    return (
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
            <Text>Jenis Instansi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Dropdown
                control={control}
                rules={{required: true}}
                name="instance_type"
                defaultValue={detailIdent?.milling_data?.instance_type??""}
                data={[
                    {label: "Pilih", value: ""},
                    {label: "Penggilingan", value: "T1"},
                    {label: "Kelompok Tani", value: "T2"}
                ]}
            />
            {errors.instance_type && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nama Instansi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input 
                control={control} 
                rules={{required: true}} 
                name="instance_name"
                defaultValue={detailIdent?.milling_data?.instance_name??""}
            /> 
            {errors.instance_name && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nama<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="name"
                defaultValue={detailIdent?.milling_data?.name??""}
            />
            {errors.name && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nomor KTP/NPWP<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                name="ktp_npwp"
                defaultValue={detailIdent?.milling_data?.ktp_npwp??""}
            />
            <Gap height={20} />
            <Text>Status<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Dropdown
                control={control}
                rules={{required: true}}
                name="status"
                defaultValue={detailIdent?.milling_data?.status??""}
                data={[
                    {label: "Pilih", value: ""},
                    {label: "Pemilik", value: "S1"},
                    {label: "Operator", value: "S2"},
                    {label: "Penanggung Jawab", value: "S3"},
                    {label: "Lainnya", value: "S4"}
                ]}
            />
            {errors.status && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            {/* <Text>Alamat</Text>
            <Gap height={10} />
            <Controller
                control={control}
                rules={{required: true}}
                name="address"
                defaultValue={detailIdent?.milling_data?.address??""}
            />
            {errors.address && <Text style={styles.validationText}>This is required.</Text>} */}
            {/* <Gap height={20} /> */}
            <Text>Kabupaten / Kota<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="city"
                defaultValue={detailIdent?.milling_data?.city??""}
            />
            {errors.city && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Provinsi<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="province"
                defaultValue={detailIdent?.milling_data?.province??""}
            />
            {errors.province && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Nomor Telepon<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="phone"
                defaultValue={detailIdent?.milling_data?.phone??""}
            />
            {errors.phone && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Kapasitas Penggilingan<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="milling_capacity"
                defaultValue={detailIdent?.milling_data?.milling_capacity??""}
            />
            {errors.milling_capacity && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Kapasitas Kerja Penggilingan (per hari)<Text style={styles.requireLbl}>*</Text></Text>
            <Gap height={10} />
            <Input
                control={control}
                rules={{required: true}}
                name="milling_work_capacity_perday"
                defaultValue={detailIdent?.milling_data?.milling_work_capacity_perday??""}
            />
            {errors.milling_work_capacity_perday && <Text style={styles.validationText}>This is required.</Text>}
            <Gap height={20} />
            <Text>Merek Dagang Beras</Text>
            <Gap height={10} />
            <Input
                control={control}
                name="rice_trademark"
                defaultValue={detailIdent?.milling_data?.rice_trademark??""}
            />
            <Gap height={20} />
            <Text>Riwayat Perbaikan</Text>
            <Gap height={10} />
            <Input
                control={control}
                placeholder="Tempat Perbaikan"
                name="history_service_place"
                defaultValue={detailIdent?.milling_data?.history_service_place??""}
            />
            <Gap height={10} />
            <Input
                control={control}
                placeholder="Jenis Perbaikan"
                name="history_service_type"
                defaultValue={detailIdent?.milling_data?.history_service_type??""}
            />
            <Text style={{...styles.validationText, color: 'grey'}}>Kosongkan jika belum pernah dilakukan perbaikan.</Text>
            <Gap height={20} />
            <ChangeSparePart control={control} data={detailIdent}/>
            <Gap height={20} />
            <SellingSpareParts control={control} data={detailIdent} />
            <Gap height={20} />
            <ConfigEngine control={control} data={detailIdent} setValue={setValue} />
            <Gap height={20} />
            <NeedSpareParts control={control} data={detailIdent}/>
            <Gap height={20} />
            <Text>Catatan</Text>
            <Gap height={10} />
            <TextArea
                control={control}
                name="note"
                defaultValue={detailIdent?.milling_data?.note??""}
            />
            <Gap height={20} />
        </ScrollView>
    );
}

export default MillingPage;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
    requireLbl: {
        color: 'red',
        fontSize: 11
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});
import React, {useState, useEffect} from 'react';
import { Alert, ActivityIndicator, StyleSheet, Text, TouchableOpacity, View, BackHandler } from 'react-native';
import { Gap, setLoading } from '../../../components';
import Icon from 'react-native-vector-icons/AntDesign';
import { useToast } from "react-native-toast-notifications";
import { deleteIdentification, getDtlIdentification, getListConfEngine, updateMillingIdentification, updateRegularIdentification } from "../../../provider/indentification_service";
import MillingPage from './mailings';
import { useForm } from 'react-hook-form';
import RegularPage from './regular';

const IdentificationDetail = ({navigation, route}) => {
    let {id} = route.params;
    const { control, handleSubmit, setValue, formState: { errors } } = useForm();
    const toast = useToast();
    const [preload, setPreload] = useState(true);
    const [detailIdent, setDetailIdent] = useState(null);
    const [engineConf, setEngineConf] = useState([]);

    useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            console.log("back")
            backPrevent();
            return true;
        });
        getDetailIdentifikasi();
        return () => backHandler.remove()
    }, [navigation]);

    const backPrevent = () => {
        Alert.alert('Apakah anda yakin?', 'Kembali akan menghilangkan perubahan yang belum disimpan.', [
            {text: 'Batal', onPress: () => {}},
            {text: 'Kembali', onPress: () => navigation.goBack()},
        ]);
    }

    const getDetailIdentifikasi = async () => {
        try {
            setDetailIdent(await getDtlIdentification(id));
            setValue("identification_id", id);
            await getEngineConf();
            setPreload(false);
        } catch (error) {
            setPreload(false);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
            }else {
                toast.show(error.toString());
            }
        }
    }

    const getEngineConf = async () => {
        try {
            setEngineConf(await getListConfEngine());
        } catch (error) {
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
            }else {
                toast.show(error.toString());
            }
        }
    }

    const deleteAction = () => {
        Alert.alert('Hapus Data', 'Apakah anda yakin ingin hapus data ini?', [
            {text: 'Batal', onPress: () => {}},
            {text: 'Hapus', onPress: doDelete},
        ]);
    }

    const doDelete = async () => {
        try {
            setLoading(true);
            await deleteIdentification(id);
            setLoading(false);
            navigation.goBack();
            toast.show("Berhasil menghapus data");
        } catch (error) {
            setLoading(false);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
            }else {
                toast.show(error.toString());
            }
        }
    }

    const onSubmit = async data => {
        if (Object.keys(data).length < 2)
            return toast.show("Mohon lengkapi data!");
        
        // console.log(detailIdent.milling, data);
        // return;

        try {
            setLoading(true);
            if (detailIdent.milling) {
                await updateMillingIdentification(data);
            }else {
                await updateRegularIdentification(data);
            }
            setLoading(false);
            navigation.goBack();
            toast.show("Berhasil menyimpan data");
        } catch (error) {
            console.log(error);
            setLoading(false);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
            }else {
                toast.show(error.toString());
            }
        }
    }


    if (preload) {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.back}>Back</Text>
                    </TouchableOpacity>
                    <Gap height={20} />
                    <Text style={styles.title}>Detail Identifikasi</Text>
                </View>
                <View style={{...styles.container, justifyContent: 'center', alignContent: 'center'}}>
                    <ActivityIndicator size="large" color="#2E2D98" />
                </View>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={backPrevent}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>Detail Identifikasi</Text>
            </View>
            <View style={styles.container}>
                {(detailIdent != null && detailIdent.milling === true) && <MillingPage data={{detailIdent, engineConf}} control={control} errors={errors} setValue={setValue}/>}
                {(detailIdent != null && detailIdent.milling === false) && <RegularPage data={detailIdent} control={control} errors={errors} setValue={setValue}/>}
                <View style={{
                    flexDirection: 'row',
                    marginVertical: 10
                }}>
                    <TouchableOpacity activeOpacity={.7} style={styles.btnAction} onPress={deleteAction}>
                        <Icon name="delete" size={20} color="red" />
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={styles.btnAction} onPress={() => navigation.navigate('IdentificationForm', {id})}>
                        <Icon name="edit" size={20} color="white" />
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={styles.btnSubmit} onPress={handleSubmit(onSubmit)}>
                        <Text style={{color: 'white'}}>SIMPAN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

export default IdentificationDetail;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    header: {
        paddingTop: 20,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    back: {
        color: '#D32421',
    },
    container: {
        flex: 1,
        paddingHorizontal: 10,
    },
    btnAction: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        borderRadius: 10,
        marginHorizontal: 3,
        backgroundColor: '#2E2D98'
    },
    btnSubmit: {
        justifyContent: 'center',
        alignItems: 'center',
        // width: 50,
        height: 50,
        borderRadius: 10,
        marginHorizontal: 3,
        backgroundColor: '#2E2D98',
        flex: 1
    }
});
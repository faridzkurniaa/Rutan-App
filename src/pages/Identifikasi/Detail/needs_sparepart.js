import React, {useEffect} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Gap, Input } from '../../../components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useFieldArray } from 'react-hook-form';

const NeedSpareParts = ({control, data}) => {
    const {fields, append, remove} = useFieldArray({
        control,
        name: "spare_part_needs"
    });

    useEffect(() => {
        populateNeedSparePart();
        return () => {}
    }, [data]);

    const populateNeedSparePart = () => {
        let {spare_part_needs} = data;
        if (spare_part_needs != null) {
            spare_part_needs.forEach((item) => {
                setTimeout(() => {
                    append({
                        "product_code": item.product_code,
                        "part_number": item.part_number,
                        "product_name": item.product_name,
                        "qty": item.qty.toString(),
                    });
                }, 500);
            });
        }
    }

    return (
        <>
            <View style={{
                flexDirection: 'row'
            }}>
                <Text style={{flex: 1}}>Daftar Kebutuhan Spare part (per Quartal)</Text>
                <TouchableOpacity activeOpacity={.7} onPress={() => {
                    append({
                        "product_code": "",
                        "part_number": "",
                        "product_name": "",
                        "qty": "0",
                    })
                }}>
                    <Icon name="plus" size={20} color="#2E2D98" />
                </TouchableOpacity>
            </View>
            <Gap height={10} />
            {fields.map((item, key) => {
                return (
                    <View style={styles.card} key={`history${key}`}>
                        <View style={{
                            flexDirection: 'row',
                            paddingHorizontal: 10
                        }}>
                            <Text style={{flex: 1}}>Spare Part {key+1}</Text>
                            <TouchableOpacity activeOpacity={.7} onPress={() => remove(key)}>
                                <Icon name="trash" color="red" size={15}/>
                            </TouchableOpacity>
                        </View>
                        <Gap height={10} />
                        <Input control={control} placeholder="Kode Produk" name={`spare_part_needs.${key}.product_code`} defaultValue={item?.product_code??""} />
                        <Gap height={10} />
                        <Input control={control} placeholder="Nomor Part" name={`spare_part_needs.${key}.part_number`} defaultValue={item?.part_number?.toString()??""}/>
                        <Gap height={10} />
                        <Input control={control} placeholder="Nama Produk" name={`spare_part_needs.${key}.product_name`} defaultValue={item?.product_name??""} />
                        <Gap height={10} />
                        <Input control={control} placeholder="Jumlah" name={`spare_part_needs.${key}.qty`} defaultValue={item?.qty?.toString()??""} keyboardType="number-pad"/>
                    </View>
                );
            })}
        </>
    );
}

export default NeedSpareParts;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});


import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Gap, Input } from '../../../components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useFieldArray } from 'react-hook-form';

const SellingSpareParts = ({control, data}) => {
    const {fields, append, remove} = useFieldArray({
        control,
        name: "spare_part_selling_histories"
    });

    useEffect(() => {
        populateSellingSparePart();
        return () => {}
    }, [data]);

    const populateSellingSparePart = () => {
        let {spare_part_selling_histories} = data;
        if (spare_part_selling_histories != null) {
            spare_part_selling_histories.forEach((item) => {
                setTimeout(() => {
                    append({
                        "spare_part_name": item.spare_part_name,
                        "spare_part_qty": item.spare_part_qty.toString()
                    });
                }, 500);
            });
        }
    }

    return (
        <>
            <View style={{
                flexDirection: 'row'
            }}>
                <Text style={{flex: 1}}>Penjualan Spare part (saat kunjungan)</Text>
                <TouchableOpacity activeOpacity={.7} onPress={() => {
                    append({
                        "spare_part_name": "",
                        "spare_part_qty": ""
                    });
                }}>
                    <Icon name="plus" size={20} color="#2E2D98" />
                </TouchableOpacity>
            </View>
            <Gap height={10} />
            {fields.map((value, key) => {
                return (
                    <View style={styles.card} key={`history${key}`}>
                        <View style={{
                            flexDirection: 'row',
                            paddingHorizontal: 10
                        }}>
                            <Text style={{flex: 1}}>Spare Part {key+1}</Text>
                            <TouchableOpacity activeOpacity={.7} onPress={() => remove(key)}>
                                <Icon name="trash" color="red" size={15}/>
                            </TouchableOpacity>
                        </View>
                        <Gap height={10} />
                        <Input control={control} placeholder="Nama Spare part" name={`spare_part_selling_histories.${key}.spare_part_name`} defaultValue={value?.spare_part_name??""} />
                        <Gap height={10} />
                        <Input control={control} placeholder="Jumlah" name={`spare_part_selling_histories.${key}.spare_part_qty`} defaultValue={value?.spare_part_qty?.toString()??""} keybaordType="number-pad"/>
                    </View>
                );
            })}
        </>
    );
}

export default SellingSpareParts;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
});


import React, {useEffect, useState, useRef} from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Button, ChecklistBox, Gap, setLoading } from "../../../components";
import { useForm, Controller } from 'react-hook-form';
import {Picker} from '@react-native-picker/picker';
import { useToast } from "react-native-toast-notifications";
import { getListBranch, getListCustomer, createIdentification, getDtlIdentification, updateIdentification } from "../../../provider/indentification_service";

const IdentificationForm = ({navigation, route}) => {
    const toast = useToast();
    let {id} = route.params;
    const cmbLocationRef = useRef();
    const { control, handleSubmit, setValue, formState: { errors } } = useForm();
    const [customer, setCustomer] = useState([]);
    const [branch, setBranch] = useState([]);

    const locationData = [
        {value: 'L1', label: 'Cabang'},
        {value: 'L2', label: 'Depo'},
    ];

    const typeData = [
        {value: 'T1', label: 'Project'},
        {value: 'T2', label: 'PB'},
        {value: 'T3', label: 'Komersil'},
    ];
    
    useEffect(() => {
        getData();
        return () => {}
    }, [navigation]);

    const getData = async () => {
        try {
            setLoading(true);
            setBranch(await getListBranch());
            setCustomer(await getListCustomer());
            if (id != null) {
                getExsistingData();
            }else {
                setLoading(false);
            }
        } catch (error) {
            console.log(error);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
              }else {
                toast.show(error.toString());
              }
        }
    }

    const getExsistingData = async () => {
        try {
            const data = await getDtlIdentification(id);
            setValue("branch_id", data.branch_id);
            setValue("customer_id", data.customer_id);
            setValue("location", locationData.filter(({label}) => data.location == label)[0].value);
            setValue("type", typeData.filter(({label}) => data.type == label)[0].value);
            setValue("milling", data.milling);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
              }else {
                navigation.goBack();
                toast.show(error.toString());
              }
        }
    }

    const onSubmit = async data => {
        try {
            
            setLoading(true);
            data['milling'] = data.milling.toString();
            if (id == null) {
                const res = await createIdentification(data);
                console.log(res);
            }else {
                data['id'] = id;
                const res = await updateIdentification(data);
                console.log(res);
            }
            setLoading(false);
            navigation.goBack();
            toast.show("Berhasil menyimpan data");
        } catch (error) {
            setLoading(false);
            console.log(error);
            if (error == "Invalid access token") {
                clearSession();
                navigation.replace("SignIn");
              }else {
                toast.show(error.toString());
              }
        }
    }
    

    return (
        <View style={styles.scaffold}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
                <Gap height={20} />
                <Text style={styles.title}>{id == null ? "Tambah Identifikasi" : "Ubah Identifikasi"}</Text>
            </View>
            <View style={styles.container}>
                <Gap height={20} />
                <Text>Cabang</Text>
                <Gap height={10} />
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="branch_id"
                    defaultValue=""
                    render={({field: { onChange, value }}) => (
                        <Picker
                            selectedValue={value}
                            onValueChange={onChange}
                            style={styles.input}
                            enabled={branch.length > 0}
                        >
                            <Picker.Item label={"Pilih Cabang"} value={""} />
                            {branch.map((v) => <Picker.Item key={v.id} label={v.name} value={v.id} />)}
                        </Picker>
                    )}
                />
                {errors.branch_id && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={20} />
                <Text>Customer</Text>
                <Gap height={10} />
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="customer_id"
                    defaultValue=""
                    render={({field: { onChange, value }}) => (
                        <Picker
                            selectedValue={value}
                            onValueChange={onChange}
                            style={styles.input}
                            enabled={customer.length > 0}
                        >
                            <Picker.Item label={"Pilih Customer"} value={""} />
                            {customer.map((v) => <Picker.Item key={v.id} label={v.name} value={v.id} />)}
                        </Picker>
                    )}
                />
                {errors.branch_id && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={20} />
                <Text>Lokasi</Text>
                <Gap height={10} />
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="location"
                    defaultValue=""
                    render={({field: { onChange, value }}) => (
                        <Picker
                            selectedValue={value}
                            onValueChange={onChange}
                            style={styles.input}
                        >
                            <Picker.Item label={"Pilih Lokasi"} value={""} />
                            {locationData.map((v) => <Picker.Item key={v.value} label={v.label} value={v.value} />)}
                        </Picker>
                    )}
                />
                {errors.location && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={20} />
                <Text>Tipe</Text>
                <Gap height={10} />
                <Controller
                    control={control}
                    rules={{required: true}}
                    name="type"
                    defaultValue=""
                    render={({field: { onChange, value }}) => (
                        <Picker
                            selectedValue={value}
                            onValueChange={onChange}
                            style={styles.input}
                        >
                            <Picker.Item label={"Pilih Tipe"} value={""} />
                            {typeData.map((v) => <Picker.Item key={v.value} label={v.label} value={v.value} />)}
                        </Picker>
                    )}
                />
                {errors.type && <Text style={styles.validationText}>This is required.</Text>}
                <Gap height={20} />
                <Controller
                    control={control}
                    name="milling"
                    defaultValue={false}
                    render={({field: { onChange, value }}) => (
                        <ChecklistBox tipe="Penggilingan" value={value} onValueChange={onChange}/>
                    )}
                />
                <Gap height={20} />
                <Button text="Simpan" textColor="white" onPress={handleSubmit(onSubmit)} />
            </View>
        </View>
    );
}

export default IdentificationForm;

const styles = StyleSheet.create({
    scaffold: {
        flex: 1,
    },
    header: {
        paddingTop: 20,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 25,
        color: '#2E2D98',
        fontWeight: 'bold',
        fontFamily: 'Poppins-Medium',
    },
    back: {
        color: '#D32421',
    },
    input: {
        borderWidth: 0.1,
        borderRadius: 3,
        borderColor: 'grey',
        backgroundColor: '#F2F6FF',
        paddingHorizontal: 15
    },
    container: {
        paddingHorizontal: 20,
        flex: 1
    },
    validationText: {
        fontSize: 10,
        color: 'red',
        marginLeft: 15
    },
});
import React from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { CarDummy } from '../../assets';
import { Gap } from '../../components/atoms';

const Perbaikan = ({navigation}) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={styles.back}>Back</Text>
          </TouchableOpacity>
          <Gap height={20} />
          <Text style={styles.pekerjaan}>Perkerjaan</Text>
        </View>
        <View style={styles.content}>
          <View style={styles.titleContainer}>
            <Text style={styles.texTitle}>Perbaikan</Text>
          </View>
          <Gap height={10} />
          <Text style={styles.text}>Type Mesin</Text>
          <Text style={styles.subText}>Traktor Roda 4</Text>
          <Gap height={10} />
          <Text style={styles.text}>Nomor Mesin</Text>
          <Text style={styles.subText}>GHJ-12345</Text>
          <Gap height={10} />
          <Text style={styles.text}>Nomor Garansi</Text>
          <Text style={styles.subText}>987GHJ-2018</Text>
          <Gap height={10} />
          <Text style={styles.text}>Kerusakan</Text>
          <Text style={styles.subText}>Filter oli rusak</Text>
          <Gap height={10} />
          <Text style={styles.text}>Foto/detail bagian yang rusak</Text>
          <ImageBackground source={CarDummy} style={styles.cover} />
        </View>
        <Gap height={20} />
        <View style={styles.content}>
          <Gap height={10} />
          <Text style={styles.text}>Nama</Text>
          <Text style={styles.subText}>Pakde Har</Text>
          <Gap height={10} />
          <Text style={styles.text}>Telp/HP</Text>
          <Text style={styles.subText}>081234567890</Text>
          <Gap height={10} />
          <Text style={styles.text}>Alamat</Text>
          <Text style={styles.subText}>Desa Dermo RT05/01</Text>
          <Text style={styles.subText}>Kec. Branjeng, Kab Gresik</Text>
          <Text style={styles.subText}>Jawa Timur 78909</Text>
          <Gap height={10} />
          <Text style={styles.text}>Metode Pembelian</Text>
          <Text style={styles.subText}>Tokopedia</Text>
        </View>
        <Gap height={20} />
        <View style={styles.button}>
          <TouchableOpacity>
            <View style={styles.btnContainer}>
              <Text style={styles.btnText}>Update Pekerjaan</Text>
            </View>
          </TouchableOpacity>
          <Gap width={20} />
          <TouchableOpacity>
            <View style={styles.btnContainer2}>
              <Text style={styles.btnText2}>Tanya Pemilik</Text>
            </View>
          </TouchableOpacity>
        </View>
        <Gap height={20} />
      </View>
    </ScrollView>
  );
};

export default Perbaikan;

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  header: {
    padding: 20,
  },
  back: {
    color: '#D32421',
  },
  pekerjaan: {
    fontSize: 25,
    color: '#2E2D98',
    fontWeight: 'bold',
    fontFamily: 'Poppins-Medium',
  },
  content: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    elevation: 10,
  },
  text: {
    fontSize: 14,
    color: '#040505',
    fontFamily: 'Poppins-Light',
    marginLeft: 5,
  },
  subText: {
    fontSize: 18,
    color: '#040505',
    fontFamily: 'Poppins-Regular',
    marginLeft: 5,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  checkbox: {
    alignContent: 'center',
  },
  label: {
    margin: 8,
    marginRight: 5,
  },
  title: {
    fontSize: 16,
    color: '#040505',
    fontFamily: 'Poppins-Medium',
    marginLeft: 5,
  },
  button: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  cover: {
    height: 330,
  },
  titleContainer: {
    backgroundColor: '#F2F6FF',
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
  },
  texTitle: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#2E2D98',
    fontWeight: 'bold',
  },
  btnContainer: {
    backgroundColor: '#2E2D98',
    borderRadius: 10,
    justifyContent: 'center',
    width: 145,
    height: 50,
  },
  btnText: {
    color: 'white',
    textAlign: 'center',
  },
  btnContainer2: {
    backgroundColor: 'white',
    borderRadius: 10,
    borderColor: '#2E2D98',
    borderWidth: 0.5,
    justifyContent: 'center',
    width: 145,
    height: 50,
  },
  btnText2: {
    color: '#2E2D98',
    textAlign: 'center',
  },
});

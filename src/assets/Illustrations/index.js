import LogoSplash from './LogoSplash.svg';
import LogoPt from './logopt.svg';
import Logo from './logo.svg';
import Banner from './banner-home.svg';

export {
    LogoSplash,
    LogoPt,
    Banner,
    Logo
}
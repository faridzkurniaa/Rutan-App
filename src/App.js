import React from 'react';
import 'react-native-gesture-handler';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import { ToastProvider } from 'react-native-toast-notifications'
import { LoadingOverlay } from './components';

export default () => {
    return (
        <SafeAreaProvider>
        <ToastProvider>
            <NavigationContainer>
            <Router />
            </NavigationContainer>
        </ToastProvider>
        <LoadingOverlay />
        </SafeAreaProvider>
    );
};
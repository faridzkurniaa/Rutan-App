import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import {Pict1, Pict2, Pict3} from '../../../assets';
import ItemListPekerjaan from '../ItemListPekerjaan';

const renderTabBar = props => (
  <TabBar
    {...props}
    indicatorStyle={{backgroundColor: 'black'}}
    style={{backgroundColor: 'white'}}
    tabStyle={{borderColor: 'black'}}
    renderLabel={({route, focused, color}) => (
      <Text
        style={{
          fontFamily: 'Poppins-Medium',
          color: focused ? '#D32421' : '#788795',
        }}>
        {route.title}
      </Text>
    )}
  />
);

const DalamProses = () => {
  return (
      <View>
        <ItemListPekerjaan image={Pict1} title="Start up - Mesin Ketinting" />
        <ItemListPekerjaan
          image={Pict2}
          title="After sales service - Traktor Roda 4"
        />
        <ItemListPekerjaan
          image={Pict3}
          title="Perbaikan - Filter oli mesin Ketinting"
        />
      </View>
  );
};

const Riwayat = () => {
  return (
      <View>
        <ItemListPekerjaan image={Pict1} title="Start up - Mesin Ketinting" />
        <ItemListPekerjaan
          image={Pict2}
          title="After sales service - Traktor Roda 4"
        />
        <ItemListPekerjaan
          image={Pict3}
          title="Perbaikan - Filter oli mesin Ketinting"
        />
      </View>
    
  );
};

const PekerjaanTabSection = () => {
  const renderScene = SceneMap({
    first: DalamProses,
    second: Riwayat,
  });
  const layout = useWindowDimensions();
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Dalam Proses'},
    {key: 'second', title: 'Riwayat'},
  ]);
  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
    />
  );
};

export default PekerjaanTabSection;

const styles = StyleSheet.create({});

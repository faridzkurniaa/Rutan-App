import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Button } from '../../atoms';
import {Pict1} from '../../../assets';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from "moment";
import 'moment/locale/id';

const colorStatus = (status) => {
    switch (status) {
		case "Rejected":
			return '#C3000D';
		case "Finished":
			return 'green';
		case "Progress":
			return '#0071C5';
		case "Approved":
			return '#00af81';
		default:
			return '#C7C5E6';
    }
}

const colorType = (type) => {
    switch(type) {
        case "Repair":
            return '#f07613';
        case "Troubleshot":
            return '#C3000D';
        case "Identification":
            return '#0071C5';
        case "Pelatihan":
            return '#23d3d9';
        case "Demo":
            return '#18db28';
        default:
            return 'grey';
    }
}


const ItemListPekerjaan = ({data, onOpenChat}) => {
	moment().locale('id');
	return (
		<View style={styles.card}>
		<View style={styles.notifTime}>
			<Text>{data.no_service}</Text>
			<View style={{
				position: 'absolute',
				top: 0, right: 0,
				paddingVertical: 5,
				paddingHorizontal: 10,
				borderRadius: 5,
				backgroundColor: colorStatus(data.status),
			}}>
				<Text style={{color: 'white'}}>{data.status}</Text>
			</View>
		</View>
		<Text style={styles.title}>{data.job_perform}</Text>
		<Text style={{fontFamily: 'Poppins-Regular', paddingLeft: 7, color: colorType(data.type)}}>{data.type}</Text>
		<View style={{flex: 1}}></View>
		{/* <Text style={styles.date}>{`${moment(data.start).format('DD MMM YYYY')} - ${moment(data.due).format('DD MMM YYYY')}`}</Text> */}
		<Text style={styles.date}>{data.created_date} WIB</Text>
		<View style={styles.bottomContainer}>
			<View style={styles.leftBottom}>
                <View style={{
                    height: 43, 
                    width: 43,
                    backgroundColor: 'grey',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 10
                }}>
                    <Icon name="users" size={25} color="white" />
                </View>
                <View>
                    <Text style={{fontFamily: 'Poppins-Regular'}}>{data.customer_name}</Text>
                    <Text style={{fontFamily: 'Poppins-Regular'}}>{data.customer_address}</Text>
                </View>
			</View>
			{/* {canChat && <View style={styles.button}>
			<Button text="Chat" textColor="white" onPress={onOpenChat} />
			</View>} */}
		</View>
		</View>
	);
};

export default ItemListPekerjaan;

const styles = StyleSheet.create({
    card: {
		flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 12,
        marginVertical: 10,
        borderRadius: 12,
        elevation: 5,
        padding: 10
	},
	notifTime: {
		height: 20,
		fontFamily: 'Poppins-Regular',
		paddingLeft: 7,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	title: {
		fontWeight: 'bold',
		fontSize: 20,
		fontFamily: 'Poppins-Medium',
		paddingLeft: 7,
		// marginBottom: 60,
	},
	date: {
		fontFamily: 'Poppins-Regular',
		paddingLeft: 7,
		fontSize: 10,
		color: 'grey'
	},
	bottomContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: 7,
		marginTop: 10,
	},
	leftBottom: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	button: {
		alignItems: 'center',
		padding:10,
	},
});

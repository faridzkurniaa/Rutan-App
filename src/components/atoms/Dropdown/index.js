import React from 'react';
import { useController } from "react-hook-form";
import { Picker } from '@react-native-picker/picker';

const Dropdown = ({control, name, rules = {}, defaultValue = "", data = []}) => {
    const {
        field: { ref, ...inputProps },
        fieldState: { invalid, isTouched, isDirty },
        formState: { touchedFields, dirtyFields }
    } = useController({
        name,
        control,
        rules,
        defaultValue,
    });
    return <Picker
        selectedValue={inputProps.value}
        onValueChange={inputProps.onChange}
        style={{
            borderWidth: 0.1,
            borderRadius: 3,
            borderColor: 'grey',
            backgroundColor: '#F2F6FF',
            paddingHorizontal: 15
        }}
    >
        {data.map((v, i) => <Picker.Item key={i} label={v.label} value={v.value} />)}
    </Picker>
}

export default Dropdown;


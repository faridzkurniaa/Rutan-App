import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const IconButton = ({icon, onPress, color = "#2E2D98", iconSize = 25, style}) => {
    return (
        <TouchableOpacity onPress={onPress} style={{...style,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 15
        }}>
            <Icon name={icon} color={color} size={iconSize}/>
        </TouchableOpacity>
    );
}

export default IconButton;
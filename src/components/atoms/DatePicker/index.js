import React from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import { useController } from "react-hook-form";
import {entity} from 'simpler-state';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from "moment";
import 'moment/locale/id';

const _picker = entity(false);

const MYDatePicker = ({control, name, onSelectDate, placeholder="", rules={}, defaultValue=""}) => {
    const show = _picker.use();
    const setShow = _picker.set;

    const {
        field: { ref, ...inputProps },
        fieldState: { invalid, isTouched, isDirty },
        formState: { touchedFields, dirtyFields }
    } = useController({
        name,
        control,
        rules,
        defaultValue,
    });

    return (
        <>
            <View style={{
                flexDirection: 'row'
            }}>
                <TextInput
                    placeholder={placeholder}
                    onBlur={inputProps.onBlur}
                    onChangeText={inputProps.onChange}
                    value={inputProps.value}
                    ref={ref} 
                    style={{
                        borderWidth: 0.1,
                        borderRadius: 3,
                        borderColor: 'grey',
                        backgroundColor: '#F2F6FF',
                        paddingHorizontal: 15
                    }}
                />
                <TouchableOpacity >
                    <Icon name="calendar-alt" size={20} color="2E2D98" />
                </TouchableOpacity>
            </View>
            <DatePicker
                modal
                open={show}
                date={inputProps.value == "" ? new Date() : inputProps.value}
                onConfirm={onSelectDate}
                onCancel={() => setShow(false)}
            />
        </>
    );
}

export default MYDatePicker;


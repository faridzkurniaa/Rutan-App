import React from 'react';
import { TextInput } from 'react-native';
import { useController } from "react-hook-form";

const Input = ({control, name, placeholder="", rules={}, defaultValue="", keyboardType="default"}) => {
    const {
        field: { ref, ...inputProps },
        fieldState: { invalid, isTouched, isDirty },
        formState: { touchedFields, dirtyFields }
    } = useController({
        name,
        control,
        rules,
        defaultValue,
    });
    return <TextInput
        placeholder={placeholder}
        onBlur={inputProps.onBlur}
        onChangeText={inputProps.onChange}
        value={inputProps.value}
        ref={ref} 
        style={{
            borderWidth: 0.1,
            borderRadius: 3,
            borderColor: 'grey',
            backgroundColor: '#F2F6FF',
            paddingHorizontal: 15
        }}
        keyboardType={keyboardType}
    />;
}

export default Input;


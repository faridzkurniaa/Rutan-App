const monthName = (monthNum) => {
    const month = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    return month[(parseInt(monthNum) - 1)];
}

export {
    monthName
}
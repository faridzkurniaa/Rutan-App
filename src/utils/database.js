import SQLite from "react-native-sqlite-storage";
SQLite.DEBUG(true);
SQLite.enablePromise(true);

let DB;


export const initDB = async () => {
    return new Promise(async (resolve, reject) => {
        try {
            if (!DB) {
                console.log("Opening database ...");
                const database = await SQLite.openDatabase({name: "rutan.sqlite", createFromLocation : 1});
                console.log("Database OPEN");
                DB = database;
            }
            resolve(DB);
        } catch (error) {
            reject(error);
        }
    });
}

export const closeDB = async () => {
    if (DB) {
        console.log("Closing DB");
        DB.close()
        .then(status => {
            console.log("Database ", status);
        })
        .catch(error => {
            console.log(error);
        });
    }else {
        console.log("Database was not OPENED");
    }
}

export const query = (query) => {
    return new Promise((resolve, reject) => {
        initDB().then((db) => {
            db.executeSql(query, [], (results) => {
                resolve(results);
            }, (err) => {
                console.log("DB Error ", err);
                reject(err);
            });
        });
    });
}



import { sys_put, sys_get } from "../utils/api_client"

export const getUserProfile = async () => {
    const response = await sys_get({auth: true, endpoint: '/me'});
    if (response.status != true) throw response.message;
    return response.callback;
}

export const saveUserProfile = async (data) => {
    const response = await sys_put({auth: true, endpoint: '/me', body: data});
    if (response.status != true) throw response.message;
    return response.callback;
}
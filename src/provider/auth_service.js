import { sys_post } from "../utils/api_client"
import { saveUser } from "../utils/session";

export const loginApi = async (nik, password) => {
    const response = await sys_post({auth: false, endpoint: '/m_auth', body: {nik, password}});
    if (response.status != true) throw response.message;
    let data = response.callback;
    return await saveUser({
        nik: data.nik,
        name: data.name,
        token: data.accessToken,
        roles: JSON.stringify(data.roles.map((role) => role.name))
    });
}
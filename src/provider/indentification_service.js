import { sys_put, sys_get, sys_post, sys_del } from "../utils/api_client";

export const getLstIdentification = async (page = 1) => {
    const response = await sys_get({auth: true, endpoint: `/m_identifications?page=${page}&limit=1000`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getDtlIdentification = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_identifications/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const createIdentification = async (body) => {
    const response = await sys_post({auth: true, endpoint: `/m_identifications`, body});
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updateIdentification = async (body) => {
    const response = await sys_put({auth: true, endpoint: `/m_identifications`, body});
    if (response.status != true) throw response.message;
    return response.callback;
}

export const deleteIdentification = async (id) => {
    const response = await sys_del({auth: true, endpoint: `/m_identifications/${id}`});
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updateMillingIdentification = async (body) => {
    const response = await sys_put({auth: true, endpoint: `/m_identifications/milling`, body});
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updateRegularIdentification = async (body) => {
    const response = await sys_put({auth: true, endpoint: `/m_identifications/regular`, body});
    console.log(response);
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getListCustomer = async () => {
    const response = await sys_get({auth: true, endpoint: `/customers?limit=99999`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getListBranch = async () => {
    const response = await sys_get({auth: true, endpoint: `/branches?limit=99999`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getListConfEngine = async () => {
    const response = await sys_get({auth: true, endpoint: `/engine_confs?limit=9999`});
    if (response.status != true) throw response.message;
    return response.callback;
}
import { sys_put, sys_get, sys_post, sys_del } from "../utils/api_client";

export const getLstService = async (page = 1) => {
    const response = await sys_get({auth: true, endpoint: `/m_services?page=${page}&limit=1000`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const searchLstService = async (data, page = 1) => {
    const response = await sys_post({auth: true, endpoint: `/m_services/search?page=${page}&limit=1000`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getDtlService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getChkService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/checklist/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updateChkService = async (id, data) => {
    const response = await sys_put({auth: true, endpoint: `/m_services/checklist/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getDlyService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/daily/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const insertDlyService = async (id, data) => {
    const response = await sys_post({auth: true, endpoint: `/m_services/daily/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const deleteDlyService = async (id, id_daily_service) => {
    const response = await sys_del({auth: true, endpoint: `/m_services/daily/${id}/${id_daily_service}`})
    console.log(`/m_services/daily/${id}/${id_daily_service}`, response);
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getMdaService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/media/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const insertMdaService = async (id, data) => {
    const response = await sys_post({auth: true, endpoint: `/m_services/media/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const deleteMdaService = async (id, id_media_service) => {
    const response = await sys_del({auth: true, endpoint: `/m_services/media/${id}/${id_media_service}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const getSmrService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/summary/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updateSmrService = async (id, data) => {
    const response = await sys_put({auth: true, endpoint: `/m_services/summary/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const lstReject = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/reject/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const insertReject = async (id, data) => {
    const response = await sys_post({auth: true, endpoint: `/m_services/reject/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const setFinishService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/finish/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const setApproveService = async (id) => {    
    const response = await sys_get({auth: true, endpoint: `/m_services/approve/${id}`})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updSerialNumber = async (id, data) => {
    const response = await sys_put({auth: true, endpoint: `/m_services/updateSerial/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const lstEmployeeService = async (id) => {
    const response = await sys_get({auth: true, endpoint: `/m_services/employee/${id}`});
    if (response.status != true) throw response.message;
    console.log(response);
    return response.callback;
}

export const insEmployeeService = async (id, data) => {
    const response = await sys_post({auth: true, endpoint: `/m_services/addEmployee/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const updEmployeeService = async (id, data) => {
    const response = await sys_put({auth: true, endpoint: `/m_services/updateEmployee/${id}`, body: data})
    if (response.status != true) throw response.message;
    return response.callback;
}

export const lstEmployee = async () => {
    const response = await sys_get({auth: true, endpoint: `/employees?page=1&limit=1000`})
    if (response.status != true) throw response.message;
    return response.callback;
}
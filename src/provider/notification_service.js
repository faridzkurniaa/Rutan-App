import { sys_get } from "../utils/api_client";

export const getListNotification = async () => {
    const response = await sys_get({auth: true, endpoint: '/m_notifs?limit=9999'});
    if (response.status != true) throw response.message;
    return response.callback;
}
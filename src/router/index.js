import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import { BottomNavigator } from '../components/molecules';
import { 
  AfterSales,
  Akun,
  Beranda,
  ChatScreen,
  Notifikasi,
  Pekerjaan,
  Perbaikan,
  Pesan,
  SignIn,
  SplashScreen,
  AkunEdit,
  DetailPekerjaan,
  FormPekerjaan,
  SummaryPekerjaan,
  CatatanPekerjaan,
  FormCatatanPekerja,
  MediaPekerjaan,
  SearchPekerjaan,
  Identification,
  IdentificationDetail,
  IdentificationForm,
  RejectPekerjaan,
  FormRejectPekerjaan,
  AssignPekerjaan,
  AssignFormPekerjaan
} from '../pages';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return(
    <Tab.Navigator tabBar={props => <BottomNavigator {...props}/>} >
      <Tab.Screen name="Beranda" component={Beranda} options={{headerShown: false}}/>
      <Tab.Screen name="Pekerjaan" component={Pekerjaan} options={{headerShown: false}}/>
      <Tab.Screen name="Identifikasi" component={Identification} options={{headerShown: false}}/>
      <Tab.Screen name="Pesan" component={Pesan} options={{headerShown: false}}/>
      <Tab.Screen name="Akun" component={Akun} options={{headerShown: false}}/>
    </Tab.Navigator>
  )
}

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ChatScreen"
        component={ChatScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Notifikasi"
        component={Notifikasi}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AfterSales"
        component={AfterSales}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Perbaikan"
        component={Perbaikan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DetailPekerjaan"
        component={DetailPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="FormPekerjaan"
        component={FormPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SummaryPekerjaan"
        component={SummaryPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CatatanPekerjaan"
        component={CatatanPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="FormCatatanPekerja"
        component={FormCatatanPekerja}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MediaPekerjaan"
        component={MediaPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SearchPekerjaan"
        component={SearchPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AkunEdit"
        component={AkunEdit}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="IdentificationForm"
        component={IdentificationForm}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="IdentificationDetail"
        component={IdentificationDetail}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RejectPekerjaan"
        component={RejectPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="FormRejectPekerjaan"
        component={FormRejectPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AssignPekerjaan"
        component={AssignPekerjaan}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AssignFormPekerjaan"
        component={AssignFormPekerjaan}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;